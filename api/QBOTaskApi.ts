import axios from 'axios';
import {Setting} from '../utils/Setting';

axios.defaults.baseURL = Setting.apiUrl

export class QBOTaskApi {
	public static getBoard(customerQbId: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			axios.get(`/Task/Board?customer_qb_id=${customerQbId}&assignment_filter=&status_filter=`)
				.then((response) => {
					// let editable = response.data.lanes[0].id == 'loading' ? false : true;
					// this.setState({ data: response.data, editable: editable })
					resolve(response)
				})
				.catch(function (error) {
					console.log(error);
					reject(error)
				})
				.then(() => {
				});
		})
	}


	public static getAllCards(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			axios.get(`/Task/AllCard`)
				.then((response) => {
					console.dir(response)
					resolve(response.data)
				})
				.catch(function (error) {
					console.log(error);
					reject(error)
				})
				.then(() => {
				});
		})
	}

	public static getTemplates = (): Promise<any | void> => {
		return new Promise((resolve, reject) => {
			axios.get(`/Task/Template`)
			.then((response) => {
				resolve(response.data)
			})
			.catch(function (error) {
				reject(error)
			})
		})
	}

	public static getDashBoardTask(user: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			axios.get(`/Task/DashboardTasks?user=${user}`)
				.then((response) => {
					console.dir(response)
					resolve(response)
				})
				.catch(function (error) {
					console.log(error);
					reject(error)
				})
				.then(() => {
				});
		})
	}

	public static getTaskStatus(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			axios.defaults.headers.common = {'Authorization': `Bearer ${localStorage.getItem('token')}}`}
			axios.get(`/Task/GetTaskStatus`)
				.then((response) => {
					resolve(response)
				})
				.catch(function (error) {
					console.log(error);
					reject(error)
				})
				.then(() => {
				});
		})
	}

	public static getBoardCard(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			axios.get(`/Task/GetBoardCard`)
				.then((response) => {
					resolve(response)
				})
				.catch(function (error) {
					console.log(error);
					reject(error)
				})
				.then(() => {
				});
		})
	}

}

