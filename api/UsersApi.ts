import { Setting } from '../utils/Setting';

export class UsersApi {
	public static apiUrl = Setting.apiUrl

	public static GetUsers(email: string = ''): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}Users/GetUsers?email=${email}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				console.dir(response)
				resolve(response)
			})
		})
	}

	public static GetRoles(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}Users/GetRoles`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				console.dir(response)
				resolve(response)
			})
		})
	}

	public static updateUser(email: any, roleIds: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}Users/UpdateUser?email=${email}`, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(roleIds)
			}).then(res => {
				return res.status;
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				console.dir(response)
				resolve(response)
			})
		})
	}
}

