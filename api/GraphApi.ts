import { Setting } from '../utils/Setting';

export class GraphApi {
	public static siteId = `ascydesign.sharepoint.com,1db002f3-bdab-4162-bc90-4d555628814e,d65e3b1b-9b74-4784-8711-3a00563149a9`
	public static driveId = `b!8wKwHau9YkG8kE1VViiBThs7XtZ0m4RHhxE6AFYxSan0tgx1PyVpQo8GEs0nFgGO`
	public static rootFileId = `01WLXBDZZN67BBB6562ZFYT2IGJBX7SG45`
	public static attachmentId = '01WLXBDZ24N4HPIA3UPRGI5IRHBE5WJW7R'
	public static itemUrl = `https://graph.microsoft.com/v1.0/sites/${GraphApi.siteId}/drives/${GraphApi.driveId}/items`

	public static apiUrl = Setting.apiUrl

	public static getToken(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}MSGraph/AccessToken`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
			}).then(res => {
				console.dir(res)
				if (res.status == 200)
					return res.json();
				else
					reject(res)
			})
				.catch(error => {
					console.dir(error)
					reject(error)
				})
				.then(response => {
					resolve(response)
				})
		})

	}

	// Email: "allenc@ascydesign.onmicrosoft.com",
	//https://docs.microsoft.com/zh-tw/graph/api/user-sendmail?view=graph-rest-1.0&tabs=http
	public static sendEmail(userPrincipalName: any, accessToken: any, emailAddress: any, subject: any, content: any): Promise<number | void> {
		return new Promise((resolve, reject) => {
			fetch(`https://graph.microsoft.com/v1.0/users/${userPrincipalName}/sendMail`, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': accessToken
				}),
				body: JSON.stringify({
					"message": {
						"subject": `${subject}`,
						"body": {
							"contentType": "Text",
							"content": `${content}`
						},
						"toRecipients": [
							{
								"emailAddress": {
									"address": `${emailAddress}`
								}
							}
						]
					}
				})
			}).then(res => {
				return res.status;
			})
				.catch(error => {
					console.dir('Error:', error)
					reject(error)
				})
				.then(response => {
					console.dir(response);
					resolve(response)
				})
		})
	}

	//https://docs.microsoft.com/zh-tw/graph/api/calendar-post-events?view=graph-rest-1.0&tabs=http
	public static createEvent(userPrincipalName: any, accessToken: any, eventSubject: any, startTime: any, endTime: any): Promise<number | void> {
		return new Promise((resolve, reject) => {
			fetch(`https://graph.microsoft.com/v1.0/users/${userPrincipalName}/calendar/events`, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': accessToken
				}),
				body: JSON.stringify({
					"subject": eventSubject,
					"start": {
						"dateTime": startTime,
						"timeZone": "UTC"
					},
					"end": {
						"dateTime": endTime,
						"timeZone": "UTC"
					}
				})
			}).then(res => {
				return res.status;
			})
				.catch(error => {
					console.dir('Error:', error)
					reject(error)
				})
				.then(response => {
					console.dir(response);
					resolve(response)
				})
		})
	}

	//https://docs.microsoft.com/zh-tw/graph/api/driveitem-delete?view=graph-rest-1.0&tabs=http
	public static deleteFile(itemId: any, accessToken: any): Promise<number | void> {
		return new Promise((resolve, reject) => {
			fetch(`https://graph.microsoft.com/v1.0/sites/${this.siteId}/drives/${this.driveId}/items/${itemId}`, {
				method: 'DELETE',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': accessToken
				}),
			}).then(res => {
				return res.status;
			})
				.catch(error => {
					console.dir('Error:', error)
					reject(error)
				})
				.then(response => {
					console.dir(response);
					resolve(response)
				})
		})
	}

	//https://docs.microsoft.com/zh-tw/graph/api/driveitem-move?view=graph-rest-1.0&tabs=http
	public static moveFile(accessToken: string, folderId: string, itemId: string, itemName: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`https://graph.microsoft.com/v1.0/sites/${this.siteId}/drives/${this.driveId}/items/${itemId}`, {
				method: 'PATCH',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': accessToken
				}),
				body: JSON.stringify({
					"parentReference": {
						"id": `${folderId}`
					},
					"name": `${itemName}`
				})
			}).then(res => {
				if (res.status == 200)
					return res.status;
				else
					reject(res)
			})
				.catch(error => {
					console.dir('Error:', error)
					reject(error)
				})
				.then(response => {
					console.dir(response);
					resolve(response)
				})
		})
	}

	public static getFile(accessToken: string, url: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(url, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': accessToken
				})
			}).then(res => {
				return res.json();
			})
				.catch(error => {
					console.dir('Error:', error)
					reject(error)
				})
				.then(response => {
					console.dir(response);
					resolve(response)
				})

		})
	}

	public static createFolder(accessToken: string, url: string, folderName: string) {
		return new Promise((resolve, reject) => {
			fetch(url, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': accessToken
				}),
				body: JSON.stringify({
					"name": folderName,
					"folder": {}
				})
			}).then(res => {
				return res.json();
			})
				.catch(error => {
					console.dir('Error:', error)
					reject(error)
				})
				.then(response => {
					console.dir(response);
					resolve(response)
				})

		})
	}

	public static async getUsers() {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}MSGraph/Users`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
			}).then(res => {
				console.dir(res)
				if (res.status == 200)
					return res.json();
				else
					reject(res)
			})
				.catch(error => {
					console.dir(error)
					reject(error)
				})
				.then(response => {
					console.dir(response)
					resolve(response)
				})
		})
	}

	public static async getCurrentUser() {
		if (this.apiUrl == `http://localhost/SharePoint/api/` || this.apiUrl == `https://localhost:44398/api/`) {
			let testUser = {
				Email: "garyl@ascydesign.onmicrosoft.com",
				// Email: "allenc@ascydesign.onmicrosoft.com",
				Id: 13,
				IsEmailAuthenticationGuestUser: false,
				IsHiddenInUI: false,
				IsShareByEmailGuestUser: false,
				IsSiteAdmin: true,
				LoginName: "i:0#.f|membership|garyl@ascydesign.onmicrosoft.com",
				PrincipalType: 1,
				Title: "Gary Li",
				UserId: { NameId: "10032000e37afc14", NameIdIssuer: "urn:federation:microsoftonline" },
				UserPrincipalName: "garyl@ascydesign.onmicrosoft.com",
			}
			return testUser
		} 
	}
}

