/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  moduleNameMapper: {
    "office-ui-fabric-react/lib/(.*)$": "office-ui-fabric-react/lib-commonjs/$1"
  }
}
