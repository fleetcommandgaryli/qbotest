export class Setting {
	public static clientPage = 'https://ascydesign.sharepoint.com/SitePages/client.aspx'
	public static dashboardPage = 'https://ascydesign.sharepoint.com/SitePages/home.aspx'
	// public static openNewTab = 'https://localhost:4321/temp/workbench.html'


	// public static apiUrl = `http://localhost/SharePoint/api/`
	public static apiUrl = 'https://fcrms.net:8443/qboApp/api/';
	// public static apiUrl = `https://localhost:44398/api/`

	public static qboConnectUrl = `https://ascydesign.sharepoint.com/SitePages/connect.aspx`

	public static periodStart = '2021-04-05'
	public static periodNumber = 14
	public static version = '1.2.2'

	public static tokenExpired = 58
	public static tokenRemindExpired = 5
	public static tokenAutouUpdate = 10

	// public static tokenExpired = 3
	// public static tokenRemindExpired = 2
	// public static tokenAutouUpdate = 1	
}

