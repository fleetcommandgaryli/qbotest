export class sessionStorageListener {
	static init() {
		console.dir('sessionStorageListener start');
		sessionStorage.setItem = (Orgin => {
			return function (key, value) {
				let setItemEvent = new CustomEvent('setItemEvent', { detail: { key, value } })
				window.dispatchEvent(setItemEvent)
				Orgin.call(this, key, typeof value == 'string' ? value : JSON.stringify(value))
			}
		})(sessionStorage.setItem)

		// console.dir('start')
		// let orignalSetItem = sessionStorage.setItem;
		// sessionStorage.setItem = function (key, value) {  
		// var setItemEvent = new Event("setItemEvent");  
		// setItemEvent.value = value;  
		// setItemEvent.key = key;  
		// window.dispatchEvent(setItemEvent);  
		// orignalSetItem.apply(this, arguments);
		// }
		// window.addEventListener("setItemEvent", function (e) {
		//         if(e.key === 'customerId'){
		//                 return e.value
		//         }

		//         console.log(e.key); // test
		//         console.log(e.value); // 1234
		// });
	}

	static addListener(key, callback) {
		window.addEventListener('setItemEvent', function (e) {
			const detail = e.detail;
			if (detail.key === key) {
				callback(JSON.parse(detail.value))
			}
		});
	}

}