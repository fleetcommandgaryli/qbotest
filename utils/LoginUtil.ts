import moment from "moment"
import { QuickbooksApi } from "../api/QuickbooksApi"
import { RefreshToken } from "../interfaces/IQboCustomerProps"
import { Setting } from "./Setting"

export async function LoginCheck() {
	let token = localStorage.getItem('token')
	let tokenTimer = localStorage.getItem('TokenTimer')

	if (!token){
		LogOut()
		return false
	}

	if (tokenTimer == null || moment().isAfter(tokenTimer)) {
		let response: RefreshToken = await QuickbooksApi.getToken()
		let token = response.quickBooksInfo.access_token
		if (!response.tokenResponse.IsError) {
			// await QuickbooksApi.updateUserAccessToken('Gary Li', token)
			localStorage.setItem('token', token)
			localStorage.setItem('TokenTimer', moment().add(Setting.tokenExpired, 'm').format('LLLL'))
			return true
		} else {
			//go to qboConnect
			LogOut()
			return false
		}
	}else{
		return true
	}
}

export function LogOut() {
	localStorage.removeItem('company_code')
	localStorage.removeItem('userInfo')
	localStorage.removeItem('token')
	window.location.href = '/'
}

