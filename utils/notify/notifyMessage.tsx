import * as React from 'react';
import styles from '../../styles/notifyMessage.module.scss';
import { mergeStyles } from '@fluentui/react';
import { FontIcon } from '@fluentui/react';
import { AnimationStyles } from '@fluentui/react';
import style from './notify.module.scss'

interface INotifyProps {
    str: string;
    class: string
}

const fadeIn = mergeStyles(AnimationStyles.fadeIn400);

export class NotifyMessage extends React.Component<INotifyProps> {
    private myRef: React.RefObject<HTMLDivElement>;
    constructor(props: INotifyProps) {
        super(props);
        this.myRef = React.createRef();
        this.closeMessage = this.closeMessage.bind(this);
    }

    closeMessage() {
        this.myRef.current.className = styles.hide
    }

    public render(): React.ReactElement<INotifyProps> {

        const iconClass = mergeStyles({
            fontSize: 16,
            height: 16,
            width: 18,
            position: 'absolute',
            right: 20,
            top: 20,
            cursor: 'pointer',
        });

        const iconTitle = mergeStyles({
            fontSize: 16,
            height: 16,
            width: 16,
            display: 'inline-block',
            fontWeight: '700'
        });

        let classString = ''
        if (this.props.str == '' || this.props.str == undefined) {
            classString = `${styles.hide}`
        } else {
            classString = `${styles.notify} ${this.props.class == 'success' ? styles.success : styles.error} ${styles["fade-out"]}`
        }
        return (
            <div ref={this.myRef} className={`${classString} ${fadeIn}`}>
                <div>
                    <div className={`${iconTitle} ${style.icon}`}>
                        <FontIcon iconName={this.props.class == 'success' ? 'Completed' : "Error"} />
                    </div>
                    <span className={styles["notify-title"]}>{this.props.class == 'success' ? 'Success' : "Error"}</span>
                </div>

                <div onClick={this.closeMessage} className={`${iconTitle} ${style.cleanIcon}`}>
                    <FontIcon iconName="Clear" />
                </div>

                <div>{
                    this.props.str ?
                    this.props.str.split('<br>').map((str, index) => {
                        return <p className={styles['notify-br']} key={index}>{str}</p>
                    }): <></>
                }</div>
            </div>
        );
    }
}

export function HookTest(props) {
    console.dir(props.testPop)
    const [count, setCount] = React.useState(0)

    console.dir(count)
    console.dir(setCount)

    const iconClass = mergeStyles({
        fontSize: 16,
        height: 16,
        width: 18,
        position: 'absolute',
        right: 10,
        top: 10,
        cursor: 'pointer',
    });

    return (
        <div>
            <p>Click {count} time</p>
            <button onClick={() => setCount(count + 1)}>
                Click me {props.testPop}
            </button>
        </div>
    )

}

export function Loading(props) {
    return (
        <div className={styles["sk-fading-circle"]}>
            <div className={`${styles["sk-circle"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle2"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle3"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle4"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle5"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle6"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle7"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle8"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle9"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle10"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle11"]}`}></div>
            <div className={`${styles["sk-circle"]} ${styles["sk-circle12"]}`}></div>
        </div>
    )

}

export function toDecimal2(x) {
    var f_x = parseFloat(x);
    if (isNaN(f_x)) {
        return false;
    }
    var f_x = Math.round(x * 100) / 100;
    var s_x = f_x.toString();
    var pos_decimal = s_x.indexOf('.');
    if (pos_decimal < 0) {
        pos_decimal = s_x.length;
        s_x += '.';
    }
    while (s_x.length <= pos_decimal + 2) {
        s_x += '0';
    }
    return s_x;
}
