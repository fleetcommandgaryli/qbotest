import { createStore, combineReducers} from 'redux';
import tokenReducer from './token';

const rootReducer = combineReducers({
    tokenReducer,
})

const store = createStore(rootReducer)

export type storeType  = ReturnType<typeof rootReducer>;

export default store;