// const tokenReducer = (state = 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..tIZa3UdHnrW0cX3tSEL10g.SHdX8Kj2G2XAHIgVWuqwNYn19XFYPtuurM4vB-Jv_b10ruWa-91Fqs8FpsrTbl8muyjttW8MalLqdlxElPKbnt36DBiB-lOZwnuMWH12pWaTMPF0mIcEBlw5sBZ25kpAgbFHfY-r2iW3BiDhTVOLPjTY-_S9UBYM2DcR-lkJZkZppMZ8KNOndM0eRhCIIB9KS0kHPdgoKUweafI3_1oCifRHmAJ3nvUoJfNorOb7SMfM7CM27Dwm7uhwObGwkFO0DXiuP0xIy8Mvf1YeW8EfVNL3Q8tAIw3bH6kEXNfRpOfBSMarD_3mrDnvV2B6N-ani7h1xLx300tVGTrQSL68NGliz4zQz7XHC1yUgePAwAO43qmQjYjaI83Ddg8PRdsTp3oWscK4R7z0xcUJxU6q6adHtNYnESDdcpTe0kSXGsGeFEe4bckxvlK3cZjjLbG_TG5kOQf2zvDWXF26dCrzxcGbwBYEsbNlmiTfosEsg5ch-klxgrNYvuUWDh96QYPbdk_DpTqAoT1Vbr3iXQiFTr9lMid4g7Oew9NhYtXWEj76JeQrwlyyu-Cat_Xd7_YcP1SLoVqem-SsZerzgpSRuyAs4NSHmgR1vvJE3JyCmE9mjevxmCRTvoPgHsy3uYcteRZwpTqHVDPNz6LfjjQKp9oA68GjhZv83CtSWw8_BHNnk_sGGL_S5DCfXrL9xk6T_9-W9wKX4nviXVDB1DshSR6EhcPOLEBFYOcMrsSZqgGepd35qbc6WMKi-U89DwFP.iStVx2cZdvG0sdBYxlDFUA'): String => state;
const tokenReducer = (state = '', action :ISetToken): String => {
    switch(action.type){
        case SETTOKEN :
            return action.payload.value;
        default:
            return state;
    }

};

export default tokenReducer;


const SETTOKEN = 'SETTOKEN'

interface ISetToken {
    type: typeof SETTOKEN;
    payload: {
        value: String;
    }
}

export const setToken = (value: String) : ISetToken => ({
    type: SETTOKEN,
    payload: {
        value,
    } 
})


