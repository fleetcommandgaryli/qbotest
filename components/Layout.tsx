import React, { ReactNode, useEffect, useState } from 'react'
import Link from 'next/link'
import Head from 'next/head'
import { Provider } from 'react-redux'
import store from '../reducers/configueStore'
import { mergeStyleSets, DefaultButton } from '@fluentui/react'
import { LogOut } from '../utils/LoginUtil'

type Props = {
  children?: ReactNode
  title?: string
}

type IUserInfo = {
  email?: string,
  familyName?: string,
  givenName?: string,
}

const Layout = ({ children, title = 'This is the default title' }: Props) => {
  const [companyCode, setCompanyCode] = useState('')
  const [userInfo, setUserInfo] = useState({} as IUserInfo)

  useEffect(() => {
    // sessionStorageEventListener()
    let isLogIn = localStorage.getItem('company_code')
    let localStorageUserInfo = localStorage.getItem('userInfo')
    console.dir(isLogIn)
    console.dir(localStorageUserInfo)
    if (isLogIn) {
      setCompanyCode(isLogIn)
    }

    if (localStorageUserInfo) {
      setUserInfo(JSON.parse(localStorageUserInfo))
    }
  }, [])

  function cleanOut() {
    LogOut()
    setCompanyCode('')
    setUserInfo({})
  }

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <header className={pageStyles.header}>
        {/* <header> */}
        {companyCode ?
          <div className={pageStyles.profile}>
            {/* <div> */}
            <span>{userInfo.email}</span>
            <DefaultButton onClick={cleanOut}>
              Log Out
            </DefaultButton>
          </div> : <></>
        }
        <nav className={pageStyles.nav}>
          {/* <nav> */}
          {
            companyCode != '' ?
              <>
                <Link href="/qboTasks">
                  <DefaultButton text="Tasks Board"></DefaultButton>
                </Link>

                <div className={pageStyles.seperator}>|</div>

                <Link href="/qboKanban">
                  <DefaultButton text="Customers Board"></DefaultButton>
                </Link>
              </>
              :
              <></>
          }

          {/* |{' '}
          <Link href="/qboConnect">
            <a>QBO Connect</a>
          </Link>{' '} */}
        </nav>
      </header>
      <Provider store={store}>
        {children}
      </Provider>
      <footer>

      </footer>
    </>
  )
}

export default Layout

const pageStyles = mergeStyleSets({
  header: {
    padding: "5px 12px 5px 12px",
  },
  profile: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'end'
  },
  nav: {
    //paddingLeft: '12px',
    height: '50px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  seperator: {
    margin: '0 10px 0 10px'
  },
  button: {
    background: "#23c9ff",
    color: "white"
  }
});