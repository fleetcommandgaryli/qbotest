
export interface IQboKanbanProps {
  description: string;
  spcontext: any;
}



export interface IQboKanbanStatus {
  accessToken: string;
  resourceData: any;
  data: any;
  // customerType: any;
  customers: any;
  selectCustomer: any;
  modalOpen: boolean;
  modalData: any;
  taskID: number | null;
  statusChangeId: string;
  role: any;
  getRoleFinished: boolean;
  filterStatusList: string[];
  filterEmployeeList: string[];
  filterUrgent: string[];
  taskStatus: any;
  initLoading: boolean;
  employee: any[];
  employeeChange: string;
  allSharePointUsers: any;
  searchCustomer: string;
  isTimerModalOpen: boolean;
  importModalOpen: boolean;
  customerBoardLane: any;
  boardLane: any;
}


