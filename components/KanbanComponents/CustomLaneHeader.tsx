import React from 'react';

export const CustomHeader = ({
  updateTitle, canAddLanes, onDelete, onDoubleClick, editLaneTitle, label, title, titleStyle, labelStyle, t, laneDraggable
}) => {
  return (
    <header
      style={{
        borderBottom: '0px solid #eee',
        whiteSpace: 'pre-wrap',
        paddingBottom: 6,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        minWidth: '173px'
      }}>
      <div style={{ fontSize: 14, fontWeight: 'bold' }}>{title}</div>
    </header>
  )
}

export default CustomHeader
