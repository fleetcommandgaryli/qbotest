import React from 'react';
import { MovableCardWrapper } from 'react-trello/dist/styles/Base';
import { IPersonaProps, Stack } from '@fluentui/react';
import { Persona, PersonaSize, PersonaPresence } from '@fluentui/react';
import styles from './QboKanban.module.scss';

const CustomCard = ({
  onClick,
  className,
  cardStyle,
  description,
  label,
  cardColor,
  metadata,
  onDelete,
  title,
  detail,
  taskStatusList,
}) => {
  const clickDelete = (e: { stopPropagation: () => void; }) => {
    onDelete()
    e.stopPropagation()
  }

  let color = '#605E5C'
  let backgroundColor = 'transparent'
  if (new Date(label) < new Date()) {
    color = 'white'
    backgroundColor = '#D13438'
  }

  const customCardStyle = {
    minWidth: '100%',
  }

  return (
    <MovableCardWrapper
      onClick={onClick}
      style={customCardStyle}
      className={className}
    >
      <header
        style={{
          borderBottom: '0px solid #eee',
          whiteSpace: 'pre-wrap',
          paddingBottom: 6,
          marginBottom: 10,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          minWidth: '100%',
        }}>
        <div style={{ fontSize: 14, fontWeight: 'bold' }}>{title}</div>
      </header>
      <div style={{ color: '#605E5C' }}>
        {description != ''
          ? <div style={{ fontSize: 14, whiteSpace: 'pre-wrap', paddingBottom: '12px' }}>
            {description}
          </div>
          : <></>
        }

        {
          taskStatusList.map((item: { board_lane_id: number; task_count: any; }, index: React.Key | null | undefined) => {
            let style = styles.defaultRemind
            if (item.board_lane_id == 99 || item.board_lane_id == 98)
              style = styles.urgentTask
            else if (item.board_lane_id == 3)
              style = styles.urgentReminder

            if (item.board_lane_id == 99 || item.board_lane_id == 98 || item.board_lane_id == 3 || item.board_lane_id == 1)
              return <div key={index} className={style}>{`${taskString(item.board_lane_id)} : ${item.task_count}`}</div>
            else
              return <></>
          })
        }

        {label &&
          <div style={{ marginTop: '20px', fontSize: 12, display: 'flex' }}>
            <div style={{ borderRadius: 3, padding: '3px', color: color, backgroundColor: backgroundColor }}>
              <i className="ms-Icon ms-Icon--Calendar"></i>&nbsp;&nbsp;
              <span tabIndex={-1}>{label}</span>
            </div>
          </div>
        }

        {metadata && metadata.length > 0 ?
          <div style={{ borderTop: '1px solid #eee', margin: '20px -10px 0 -10px', paddingTop: 6 }} >
            <Stack horizontal style={{ paddingLeft: 10 }} tokens={{ childrenGap: '10 12' }} wrap>
              {metadata.map((assigned_: JSX.IntrinsicAttributes & IPersonaProps & { children?: React.ReactNode; }) => {
                return <div><Persona
                  {...assigned_}
                  size={PersonaSize.size28}
                  presence={PersonaPresence.none}
                  presenceTitle={assigned_.title}
                  hidePersonaDetails={metadata.length > 1 ? true : false}
                /></div>
              })}
            </Stack>
          </div>
          : null
        }
      </div>
    </MovableCardWrapper >
  )
}

function taskString(id: number) {
  switch (id) {
    case 1:
      return "Pending tasks"
    // case 2:
    //   return "Completed tasks"
    // case 3:
    //   return "Reminders"CustomCard
    case 92:
      return "Low tasks"
    case 93:
      return "Low reminders"
    case 94:
      return "Medium tasks"
    case 95:
      return "Medium reminders"
    case 96:
      return "Important tasks"
    case 97:
      return "Important reminders"
    case 98:
      return "Urgent tasks"
    case 99:
      return "Urgent reminders"
    default:
      return "unknown"
  }
}

export default CustomCard;