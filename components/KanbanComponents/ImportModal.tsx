import * as React from 'react';
import { QuickbooksApi } from '../../api/QuickbooksApi';
import { useId } from '@fluentui/react-hooks';
import {
  getTheme,
  mergeStyleSets,
  FontWeights,
  ContextualMenu,
  Modal,
  IDragOptions,
  IconButton,
  IIconProps,
  PrimaryButton,
  Dropdown,
  IDropdownStyles,
  IDropdownOption
} from '@fluentui/react';
import { useMemo, useRef, useState } from 'react';
import axios from 'axios';


interface IImportModalProp {
  modalOpend: boolean;
  modalHide: any;
  boardData: any;
  reloadData: any;
  customers: any;
}

export const ImportModal: React.FunctionComponent<IImportModalProp> = (props) => {
  const initImportCustomer : any[] = [];
  const titleId = useId('title');
  const [cardLane, setCardLane] = useState('');
  const [importCustomer, setImportCustomer] = useState(initImportCustomer);
  const [boardData] = useState(props.boardData)

  const onSubmit = async () => {
    console.dir(cardLane)
    console.dir(importCustomer)
    axios.defaults.headers.common = { 'Authorization': `Bearer ${localStorage.getItem('token')}}` }

    let findLane = props.boardData.lanes.find(lane => {
      return lane.id == cardLane
    })
    let postion = findLane.cards.length
    let cards = importCustomer.map(customer => {
      const card = {
        customer_board_lane_id: cardLane,
        position: postion,
        customer_qb_id: customer
      }
      postion++

      return card
    })
    await axios.post('/Customer/Card', cards)
    setImportCustomer([])
    props.reloadData()
    props.modalHide()
  }

  const getCustomer = useMemo(
    () => {
      let selectedCusotmer: Array<string | Number> = []

      props.boardData.lanes.forEach(lane => {
        lane.cards.forEach(card => {
          selectedCusotmer.push(card.detail.Id)
        })
      })

      let options2: { key: any; text: any; }[] = [];
      let customers = props.customers
      console.dir(customers)

      if (customers) {
        customers.forEach(customer => {
          if (selectedCusotmer.indexOf(customer.Id) == -1)
            options2.push({ key: customer.Id, text: customer.PrintOnCheckName })
        })

        // for (let i = 0; i < customers.length; i++) {
        //   options2.push({ key: customers[i].Id, text: customers[i].PrintOnCheckName })
        // }
      }

      return options2
    }, [cardLane]
  )

  return (
    <div>
      <Modal
        titleAriaId={titleId}
        isOpen={props.modalOpend}
        onDismiss={modalCloseHandler}
        isBlocking={false}
        containerClassName={contentStyles.container}
      >
        <div className={contentStyles.header}>
          <span id={titleId}>Import customers from QBO</span>
          <IconButton
            styles={iconButtonStyles}
            iconProps={cancelIcon}
            ariaLabel="Close popup modal"
            onClick={modalCloseHandler}
          />
        </div>
        <div className={contentStyles.body}>
          <Dropdown
            placeholder="Select an option"
            label="Select a category"
            options={getCategory()}
            styles={dropdownStyles}
            onChange={boardLaneOnChangeHandler}
          />
          <br />
          <Dropdown
            placeholder="Select options"
            label="Select customers"
            defaultSelectedKeys={[]}
            selectedKeys={importCustomer}
            multiSelect
            options={getCustomers()}
            styles={dropdownStyles}
            onChange={customerOnChangeHandler}
          />
          <br />
          <PrimaryButton onClick={onSubmit}>Submit</PrimaryButton>
        </div>
      </Modal>
    </div>
  );

  function modalCloseHandler() {
    setImportCustomer([])
    props.modalHide()
  }

  function getCategory() {
    return props.boardData.lanes.map((item: { id: { toString: () => any; }; title: any; }) => {
      return { key: item.id.toString(), text: item.title }
    })
  }



  function getCustomers() {
    let selectedCusotmer: Array<string | Number> = []

    props.boardData.lanes.forEach(lane => {
      lane.cards.forEach(card => {
        selectedCusotmer.push(card.detail.Id)
      })
    })

    let options2: { key: any; text: any; }[] = [];
    let customers = props.customers
    console.dir(customers)

    if (customers) {
      customers.forEach(customer => {
        if (selectedCusotmer.indexOf(customer.Id) == -1)
          options2.push({ key: customer.Id, text: customer.PrintOnCheckName })
      })
    }

    return options2
  }

  function boardLaneOnChangeHandler(option, index) {
    setCardLane(index.key)
  }

  function customerOnChangeHandler(option, index) {
    console.dir(index)
    if (index.selected) {
      let tt = [...importCustomer, index.key]
      setImportCustomer([...importCustomer, index.key])
    } else {
      const findIndex = importCustomer.indexOf(index.key)
      let tmpImportCustomer = JSON.parse(JSON.stringify(importCustomer))
      const newValue = tmpImportCustomer.splice(findIndex, 1)
      setImportCustomer(tmpImportCustomer)
    }


  }
};
const dragOptions: IDragOptions = {
  moveMenuItemText: 'Move',
  closeMenuItemText: 'Close',
  menu: ContextualMenu,
};
const cancelIcon: IIconProps = { iconName: 'Cancel' };

const options: IDropdownOption[] = [
  { key: 'apple', text: 'Apple' },
  { key: 'banana', text: 'Banana' },
  { key: 'orange', text: 'Orange' },
  { key: 'grape', text: 'Grape' },
  { key: 'broccoli', text: 'Broccoli' },
  { key: 'carrot', text: 'Carrot' },
  { key: 'lettuce', text: 'Lettuce' },
];
const theme = getTheme();
const contentStyles = mergeStyleSets({
  container: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'stretch',
  },
  header: [
    theme.fonts.xLargePlus,
    {
      flex: '1 1 auto',
      borderTop: `4px solid ${theme.palette.themePrimary}`,
      color: theme.palette.neutralPrimary,
      display: 'flex',
      alignItems: 'center',
      fontWeight: FontWeights.semibold,
      padding: '12px 12px 0px 24px',
    },
  ],
  parentTitle: [
    {
      color: theme.palette.neutralPrimary,
      padding: '0px 12px 14px 24px',
    },
  ],
  body: {
    flex: '4 4 auto',
    padding: '24px 24px 24px 24px',
    overflowY: 'hidden',
    width: '700px',
    selectors: {
      p: { margin: '14px 0' },
      'p:first-child': { marginTop: 0 },
      'p:last-child': { marginBottom: 0 },
    },
  },
});

const iconButtonStyles = {
  root: {
    color: theme.palette.neutralPrimary,
    marginLeft: 'auto',
    marginTop: '4px',
    marginRight: '2px',
  },
  rootHovered: {
    color: theme.palette.neutralDark,
  },
};

const dropdownStyles: Partial<IDropdownStyles> = {
  dropdown: { width: 300 },
};
