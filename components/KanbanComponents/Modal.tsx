import * as React from 'react';
import { useId, useBoolean } from '@fluentui/react-hooks';
import {
  getTheme,
  mergeStyleSets,
  FontWeights,
  ContextualMenu,
  Toggle,
  DefaultButton,
  Modal,
  IDragOptions,
  IconButton,
  IIconProps,
  IContextualMenuItem,
  Dialog,
  DialogFooter,
  PrimaryButton,
  DialogType,
  Dropdown,
} from '@fluentui/react';
import CustomCard from './ModalCard'
import styles from './QboKanban.module.scss';
import { Loading } from '../../utils/notify/notifyMessage';
import { useState } from 'react';
import Link from 'next/link'
// import from './QboKanban.css';

const dragOptions: IDragOptions = {
  moveMenuItemText: 'Move',
  closeMenuItemText: 'Close',
  menu: ContextualMenu,
};
const cancelIcon: IIconProps = { iconName: 'Cancel' };
const linkIcon: IIconProps = { iconName: 'Link' };

interface IKanbanModalProp {
  modalOpend: boolean;
  modalShow: any;
  modalHide: any;
  modalData: any;
  customer: any;
  customers: any;
  openNewTab: any;
  setTaskId: any;
  dropDownOption: any;
  changeStatusId: string;
  selectChangeStatusId: any;
  updateCustomer: any;
  getStatusChangeId: string;
  employeeOption: any[];
  employeeChange: string;
  selectChangeEmployee: any;
  getEmployeeKey: any;
  boardLane: any;
}

export const KanbanModal: React.FunctionComponent<IKanbanModalProp> = (props) => {
  const [isDraggable, { toggle: toggleIsDraggable }] = useBoolean(false);
  const [hideCompleted, setHideCompleted] = useState(false);

  const linkRef = React.useRef(null);
  const [showContextualMenu, setShowContextualMenu] = React.useState(false);
  const onShowContextualMenu = React.useCallback((ev: React.MouseEvent<HTMLElement>) => {
    ev.preventDefault(); // don't navigate
    setShowContextualMenu(true);
  }, []);
  const onHideContextualMenu = React.useCallback(() => setShowContextualMenu(false), []);

  const dialogStyles = { main: { maxWidth: 450 } };
  const [hideDialog, { toggle: toggleHideDialog }] = useBoolean(true);
  const labelId: string = useId('dialogLabel');
  const subTextId: string = useId('subTextLabel');
  const modalProps = React.useMemo(
    () => ({
      titleAriaId: labelId,
      subtitleAriaId: subTextId,
      isBlocking: false,
      styles: dialogStyles,
    }),
    [labelId, subTextId],
  );

  const [hideEmployeeDialog, { toggle: toggleHideEmployeeDialog }] = useBoolean(true);
  const setEmployeeId: string = useId('setEmployeeId');
  const subTextEmployeeId: string = useId('subTextEmployeeLabel');
  const modalEmployeeProps = React.useMemo(
    () => ({
      titleAriaId: labelId,
      subtitleAriaId: subTextEmployeeId,
      isBlocking: false,
      styles: dialogStyles,
    }),
    [setEmployeeId, subTextEmployeeId],
  );

  const dialogContentProps = {
    type: DialogType.normal,
    title: 'Change status',
    closeButtonAriaLabel: 'Close',
    // subText: 'Do you want to send this message without a subject?',
  };

  const dialogEmployeeProps = {
    type: DialogType.normal,
    title: 'Change Employee',
    closeButtonAriaLabel: 'Close',
  };

  // Use useId() to ensure that the IDs are unique on the page.
  // (It's also okay to use plain strings and manually ensure uniqueness.)
  const titleId = useId('title');
  let parentTitle = ''
  if (props.customer.FullyQualifiedName != undefined) {
    const lastIndexOf = props.customer.FullyQualifiedName.lastIndexOf(':')
    parentTitle = props.customer.FullyQualifiedName.substr(0, lastIndexOf)
  }

  const menuItems: IContextualMenuItem[] = [
    {
      key: 'move', onClick: () => toggleHideDialog(),
      iconProps: { iconName: 'Forward', style: { color: 'black' } }, text: 'Change Status'
    },
    {
      key: 'move', onClick: () => toggleHideEmployeeDialog(),
      iconProps: { iconName: 'Forward', style: { color: 'black' } }, text: 'Change Employee'
    },

  ];

  const moreIcon: IIconProps = { iconName: 'More' };
  const linkClickHandler = () => {
    const getCustomerDetail = localStorage.getItem('customer')
    if (getCustomerDetail)
      localStorage.setItem('taskFilter', JSON.parse(getCustomerDetail).Id)
  }

  return (
    <div>
      <Modal
        titleAriaId={titleId}
        isOpen={props.modalOpend}
        onDismiss={modalCloseHandler}
        isBlocking={false}
        containerClassName={contentStyles.container}
        dragOptions={isDraggable ? dragOptions : undefined}

      >
        <div className={contentStyles.header}>
          <span id={titleId}>{props.customer.DisplayName}</span>
          <div style={{ display: 'inline-block' }}>
            <Link href="/qboTasks">
              {/* <IconButton
                styles={iconButtonStyles}
                iconProps={linkIcon}
                ariaLabel="link"
              /> */}
              <DefaultButton text="View tasks" onClick={linkClickHandler}></DefaultButton>
            </Link>{' '}
          </div>

        </div>
        <div className={`${rowStyle.row} ${rowStyle.spaceBetween}`}>
          <div className={contentStyles.parentTitle}></div>
          <div style={{ paddingRight: '38px' }} className={`${rowStyle.row}`}>

          </div>
        </div>

        <div className={contentStyles.body}>
          {
            getCardComponent()
          }

        </div>
      </Modal>

      <Dialog
        hidden={hideDialog}
        onDismiss={toggleHideDialog}
        dialogContentProps={dialogContentProps}
        modalProps={modalProps}
        minWidth='600px'
      >
        <Dropdown
          label=''
          options={props.dropDownOption}
          dropdownWidth={0}
          placeholder='Select a status'
          defaultSelectedKey={props.changeStatusId}
          onChange={(option, index) => { props.selectChangeStatusId(option, index) }}
        />

        <DialogFooter>
          <PrimaryButton onClick={clickSendHandler} text="Yes" />
          <DefaultButton onClick={toggleHideDialog} text="No" />
        </DialogFooter>
      </Dialog>

      <Dialog
        hidden={hideEmployeeDialog}
        onDismiss={toggleHideEmployeeDialog}
        dialogContentProps={dialogEmployeeProps}
        modalProps={modalEmployeeProps}
        minWidth='500px'
      >
        <Dropdown
          label='Employee:'
          style={{ marginBottom: '15px' }}
          defaultSelectedKey={props.employeeChange}
          options={props.employeeOption}
          onChange={(option, index) => { props.selectChangeEmployee(option, index) }}
        />

        <DialogFooter>
          <PrimaryButton onClick={clickEmployeeChangeHandler} text="Yes" />
          <DefaultButton onClick={toggleHideEmployeeDialog} text="No" />
        </DialogFooter>
      </Dialog>
    </div>
  );

  function getCard() {
    let result: any[] = []

    if (props.modalData.length != 0) {
      props.modalData.forEach(item => {
        item.cards.forEach(card => {
          // console.dir(card)
          if (card.customer_qb_id == props.customer.Id) {
            card.status_qb_id = card.status_qb_id == '' ? '0' : card.status_qb_id
            if (hideCompleted) {
              if (card.lane_id != '2') {
                card.className = styles.modalCardCustom
                card.onClick = () => props.setTaskId(card.id)
                card.openNewTab = props.openNewTab
                result.push(card)
              }
            } else {
              card.className = styles.modalCardCustom
              card.onClick = () => props.setTaskId(card.id)
              card.openNewTab = props.openNewTab
              result.push(card)
            }

          }

        })
      });
    }

    console.dir(result)

    result.sort(function (a: any, b: any) {
      let nameA = a.status_qb_id
      let nameB = b.status_qb_id
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    })

    return result
  }

  function getCardComponent() {
    if (props.modalData.length == 0) {
      return <Loading></Loading>
    } else {
      let cards = getCard()
      console.dir(cards)

      return (
        props.boardLane.map(boardLane => {
          if (cards.find(card => card.lane_id == boardLane.id)) {
            return (
              <div key={boardLane.id} className={styles.modalItemFlex}>
                <p className={styles.modalItemP}>
                  {boardLane.title}
                </p>
                {
                  cards.filter(card => {
                    return card.lane_id == boardLane.id
                  }).map(card => {
                    return CustomCard(card)
                  })
                }
              </div>
            )
          } else {
            return <></>
          }

        })
      )

    }

  }

  function hideCompletedHandler() {
    setHideCompleted(true)
  }

  function showCompletedHandler() {
    setHideCompleted(false)
  }

  function modalCloseHandler() {
    setHideCompleted(false)
    props.modalHide()
  }

  function clickSendHandler() {
    console.dir(props.customer)
    console.dir(props.getStatusChangeId)
    props.updateCustomer(props.customer, props.getStatusChangeId, null)
    toggleHideDialog()
  }

  function clickEmployeeChangeHandler() {
    props.updateCustomer(props.customer, null, props.employeeChange)
    toggleHideEmployeeDialog()
  }
};

const theme = getTheme();
const contentStyles = mergeStyleSets({
  container: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'stretch',
  },
  header: [
    // eslint-disable-next-line deprecation/deprecation
    theme.fonts.xLargePlus,
    {
      flex: '1 1 auto',
      borderTop: `4px solid ${theme.palette.themePrimary}`,
      color: theme.palette.neutralPrimary,
      display: 'flex',
      alignItems: 'center',
      fontWeight: FontWeights.semibold,
      padding: '12px 12px 0px 24px',
      backgroundColor: '#e3e3e3',
      justifyContent: 'space-between'
    },
  ],
  parentTitle: [
    {
      color: theme.palette.neutralPrimary,
      padding: '0px 12px 14px 24px',
      backgroundColor: '#e3e3e3',
    },
  ],
  body: {
    flex: '4 4 auto',
    padding: '0 24px 24px 24px',
    overflowY: 'hidden',
    backgroundColor: '#e3e3e3',
    width: '700px',
    selectors: {
      p: { margin: '14px 0' },
      'p:first-child': { marginTop: 0 },
      'p:last-child': { marginBottom: 0 },
    },
  },
});
const toggleStyles = { root: { marginBottom: '20px' } };
const iconButtonStyles = {
  root: {
    color: theme.palette.neutralPrimary,
    marginLeft: 'auto',
    marginTop: '4px',
    marginRight: '2px',
  },
  rootHovered: {
    color: theme.palette.neutralDark,
  },
};

const rowStyle = mergeStyleSets({
  row: {
    display: 'flex',
    background: '#e3e3e3',
  },
  spaceBetween: {
    justifyContent: 'space-between',
  },
  button: {
    textDecoration: 'none',
    height: '32px',
    minWidth: '80px',
    // backgroundColor: '#0078d4',
    borderColor: '#0078d4',
    // color: '#fff',
    outline: 'transparent',
    position: 'relative',
    // font-family: Segoe UI WestEuropean,Segoe UI,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif;
    // -webkit-font-smoothing: antialiased;
    fontSize: '14px',
    fontWeight: '400',
    borderWidth: '0',
    textAlign: 'center',
    cursor: 'pointer',
    display: 'inline-block',
    padding: '0 16px',
  },

  buttonSelect: {
    backgroundColor: '#0078d4',
    color: '#fff',
  },
  hide: {
    display: 'none',
  }
});
