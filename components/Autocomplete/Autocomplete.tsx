import React, { Component, Fragment } from "react";
import  style from './Autocomplete.module.css'
// require("./Autocomplete.css");

interface AutocompleteProps {
  suggestions: Array<AutocompleteOptions>,
  onSearch: any,
  placeholder: string,
  width: number,
  userInput: string
}

interface AutocompleteState {
  activeSuggestion: number,
  filteredSuggestions: Array<AutocompleteOptions>,
  showSuggestions: boolean,
  userInput: string | null
}

interface AutocompleteOptions {
  key: string,
  text: string
}
class Autocomplete extends Component<AutocompleteProps, AutocompleteState> {
  constructor(props: AutocompleteProps) {
    super(props);

    this.state = {
      // The active selection's index
      activeSuggestion: 0,
      // The suggestions that match the user's input
      filteredSuggestions: [],
      // Whether or not the suggestion list is shown
      showSuggestions: false,
      // What the user has entered
      userInput: null
    };
  }

  onChange = e => {
    const suggestions = this.props.suggestions
    const userInput = e.currentTarget.value;

    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = suggestions.filter(
      suggestion =>
        suggestion.text.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );
    // console.log(filteredSuggestions)
    this.setState({
      activeSuggestion: -1,
      filteredSuggestions: filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    });
  };

  onClick = e => {
    console.log(e.currentTarget.innerText)
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });
    this.props.onSearch(e.currentTarget.innerText)
  };

  onBlur = e => {
    this.setState({
      showSuggestions: false
    })
  }

  onKeyDown = e => {
    const activeSuggestion = this.state.activeSuggestion;
    const filteredSuggestions = this.state.filteredSuggestions;

    // User pressed the enter key
    if (e.keyCode === 13) {
      console.log(filteredSuggestions[activeSuggestion] ? filteredSuggestions[activeSuggestion] : e.currentTarget.value)
      this.setState({
        activeSuggestion: -1,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion] ? filteredSuggestions[activeSuggestion].text : e.currentTarget.value
      });
      this.props.onSearch(filteredSuggestions[activeSuggestion] ? filteredSuggestions[activeSuggestion] : e.currentTarget.value)
    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeSuggestion === -1) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    // User pressed the down arrow
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  public render(): React.ReactElement<AutocompleteProps> {
    return (
      <Fragment>
        <input
          className={style.autoCompalateInput}
          type="search"
          onChange={this.onChange}
          onKeyDown={this.onKeyDown}
          onBlur={this.onBlur}
          value={this.state.userInput === null ? this.props.userInput : this.state.userInput}
          placeholder={this.props.placeholder}
          style={{ width: this.props.width }}
        />
        {this.state.showSuggestions && this.state.userInput && this.state.filteredSuggestions.length ?
          <ul className={style.suggestions} style={{ width: this.props.width - 2 }}>
            {this.state.filteredSuggestions.map((suggestion, index) => {
              let className;

              // Flag the active suggestion with a class
              if (index === this.state.activeSuggestion) {
                className = style["suggestion-active"];
              }

              return (
                <li className={`${className} ${style.suggestionsFont}`} key={suggestion.key} onMouseDown={this.onClick}>
                  {suggestion.text}
                </li>
              );
            })}
          </ul>
          : null
        }
      </Fragment>
    );
  }
}

export default Autocomplete;
