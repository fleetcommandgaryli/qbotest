import * as React from 'react';
import { useId, useBoolean } from '@fluentui/react-hooks';
import {
    getTheme,
    mergeStyleSets,
    FontWeights,
    Modal,
    IconButton,
    IIconProps,
} from '@fluentui/react';
import styles from './Timer.module.scss';
import moment from 'moment';
import { useState } from 'react';
import { Setting } from '../../utils/Setting';

const cancelIcon: IIconProps = { iconName: 'Cancel' };

interface TimerModalProps {
    isModalOpen: boolean;
    modalClose: any;
    refreshToken: any;
}

export const TimerModal: React.FunctionComponent<TimerModalProps> = (props) => {
    const titleId = useId('title');
    const [expired, setExpired] = useState('');

    setInterval(() => {
        setExpired(getExpiredTime())
    }, 1000);

    return (
        <div>
            <Modal
                titleAriaId={titleId}
                isOpen={props.isModalOpen}
                // onDismiss={props.modalClose}
                isBlocking={false}
                containerClassName={contentStyles.container}
            >
                <div className={contentStyles.header}>
                    <span id={titleId}>{expired.indexOf('-') === -1 ? 'Session timeout warning' : 'Session timeout'}</span>
                </div>
                <div className={contentStyles.body}>
                    {
                        expired.indexOf('-') === -1 ?
                            <div>
                                <p>Your session will expire in {expired}</p>
                                <p>Click "Keep working" if you want to continue.</p>                                
                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                    {/* <button style={styleCancelButton} className={styles.button} onClick={props.modalClose}>No</button> */}
                                    <button style={styleButton} className={styles.button} onClick={props.refreshToken}>Keep working</button>
                                </div>
                            </div>
                            :
                            <div>
                                <p>Your session has expired. You will be redirected to the front page.</p>
                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                    <button style={styleButton} className={styles.button} onClick={toDashboard}>Ok</button>
                                </div>
                            </div>
                    }
                </div>
            </Modal>
        </div>
    );
};

function toDashboard() {
    window.location.href = Setting.dashboardPage
}


function getExpiredTime() {
    let now = moment()
    const expired = moment(localStorage.getItem('TokenTimer'))
    let second = (expired.diff(now, 's') % 60).toString()
    second = second.length === 1 ? `0${second}` : second
    return `${expired.diff(now, 'm')}:${second}`
}

const theme = getTheme();
const contentStyles = mergeStyleSets({
    container: {
        display: 'flex',
        flexFlow: 'column nowrap',
        alignItems: 'stretch',
    },
    header: [
        // eslint-disable-next-line deprecation/deprecation
        theme.fonts.xLargePlus,
        {
            flex: '1 1 auto',
            borderTop: `4px solid ${theme.palette.themePrimary}`,
            color: theme.palette.neutralPrimary,
            display: 'flex',
            alignItems: 'center',
            fontWeight: FontWeights.semibold,
            padding: '12px 12px 14px 24px',
        },
    ],
    body: {
        flex: '4 4 auto',
        padding: '0 24px 24px 24px',
        overflowY: 'hidden',
        selectors: {
            p: { margin: '14px 0' },
            'p:first-child': { marginTop: 0 },
            'p:last-child': { marginBottom: 0 },
        },
    },
});
const iconButtonStyles = {
    root: {
        color: theme.palette.neutralPrimary,
        marginLeft: 'auto',
        marginTop: '4px',
        marginRight: '2px',
    },
    rootHovered: {
        color: theme.palette.neutralDark,
    },
};

const styleCancelButton = {
    cursor: 'pointer',
    backgroundColor: '#eee',
    borderColor: '#eee',
    color: 'black',
    display: 'inline-block',
    borderWidth: '0',
    padding: '6px 16px',
    outline: 'transparent',
    margin: '10px',

}

const styleButton = {
    cursor: 'pointer',
    backgroundColor: '#0078d4',
    borderColor: '#0078d4',
    color: 'white',
    display: 'inline-block',
    borderWidth: '0',
    padding: '6px 16px',
    outline: 'transparent',
    margin: '10px',

}
