import React, { useState } from 'react';
import { useId, useBoolean } from '@fluentui/react-hooks';
import Comment from '../Comment/Comment';
import Checklist from '../Checklist/Checklist'
import {
  getTheme, mergeStyleSets, FontWeights, ContextualMenu, DefaultButton, Modal, IDragOptions,
  IconButton, IIconProps, ActionButton, Callout, DirectionalHint,
} from '@fluentui/react';
import { TextField, ITextField } from '@fluentui/react';
import { Stack, IStackProps, IStackStyles, IStackTokens } from '@fluentui/react';
import { Text } from '@fluentui/react';
import { Persona, PersonaSize, PersonaPresence } from '@fluentui/react';
import { Calendar, DayOfWeek } from '@fluentui/react';
import { FocusTrapZone } from '@fluentui/react';
import { IRefObject } from '@fluentui/react';
import { Dropdown, IDropdownOption, IDropdown, DropdownMenuItemType, IDropdownStyles } from '@fluentui/react';
import { Icon } from '@fluentui/react';
import { IContextualMenuItem, IContextualMenuProps } from '@fluentui/react';
import { Label } from '@fluentui/react';
import { Checkbox } from '@fluentui/react';
import { ProgressIndicator } from '@fluentui/react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { Dialog, DialogType, DialogFooter } from '@fluentui/react';
import { GraphApi } from '../../../api/GraphApi';


// require('./Modal.css')
require('office-ui-fabric-core/dist/css/fabric.min.css')

const theme = getTheme();

const dragOptions: IDragOptions = {
  moveMenuItemText: 'Move',
  closeMenuItemText: 'Close',
  menu: ContextualMenu,
};


const priorityOptions = [
  { key: '1', text: 'Urgent', data: { icon: 'Ringer', colorName: "#CF363D" } },
  { key: '2', text: 'Important', data: { icon: 'Important', colorName: "#CF363D" } },
  { key: '3', text: 'Medium', data: { icon: 'LocationDot', colorName: "#629AA7" } },
  { key: '4', text: 'Low', data: { icon: 'Down', colorName: "#A7C7D9" } }
]

const categoryOptions = [
  { key: '1', text: 'Current tasks' },
  { key: '2', text: 'Completed' },
  { key: '3', text: 'Reminders' },
]

const onRenderOption = (option: IDropdownOption): JSX.Element => {
  return (
    <div>
      {option.data && option.data.icon && (
        <Icon style={{ marginRight: '8px', color: option.data.colorName }} iconName={option.data.icon} aria-hidden="true" title={option.data.icon} />
      )}
      <span>{option.text}</span>
    </div>
  );
};

const onRenderTitle = (options: IDropdownOption[]): JSX.Element => {
  const option = options[0];
  return (
    <div>
      {option.data && option.data.icon && (
        <Icon style={{ marginRight: '8px', color: option.data.colorName }} iconName={option.data.icon} aria-hidden="true" title={option.data.icon} />
      )}
      <span>{option.text}</span>
    </div>
  );
};
const fileTypeImage = 'https://static2.sharepointonline.com/files/fabric/assets/item-types/48/'
const cancelIcon: IIconProps = { iconName: 'Cancel' };
const moreIcon: IIconProps = { iconName: 'More' };
const iconProps = { iconName: 'Calendar' };
const addFriendIcon: IIconProps = { iconName: 'AddFriend' };
const vStackTokens: IStackTokens = { childrenGap: 10 };
const stackTokens = { childrenGap: 20 };
const stackStyles: Partial<IStackStyles> = { root: { width: 650 } };
const columnProps: Partial<IStackProps> = {
  tokens: { childrenGap: 3 },
  styles: { root: { width: 650 } },
};
let calendarButtonElement: HTMLElement;

const DayPickerStrings = {
  months: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
  goToToday: 'Go to today',
  weekNumberFormatString: 'Week number {0}',
  prevMonthAriaLabel: 'Previous month',
  nextMonthAriaLabel: 'Next month',
  prevYearAriaLabel: 'Previous year',
  nextYearAriaLabel: 'Next year',
  prevYearRangeAriaLabel: 'Previous year range',
  nextYearRangeAriaLabel: 'Next year range',
  closeButtonAriaLabel: 'Close',
  monthPickerHeaderAriaLabel: '{0}, select to change the year',
  yearPickerHeaderAriaLabel: '{0}, select to change the month',
};
const dialogStyles = { main: { maxWidth: 450 } };
const dialogContentProps = {
  type: DialogType.normal,
  title: 'Move task',
  closeButtonAriaLabel: 'Close',
};
const dialogReminderProps = {
  type: DialogType.normal,
  title: 'Set reminder',
  closeButtonAriaLabel: 'Close',
};

export const CardModal = (props) => {
  const [isTextFieldBorderless, { setTrue: hideBorder, setFalse: showBorder }] = useBoolean(true);
  const [isTextFieldReadonly, { setTrue: enableReadonly, setFalse: disableReadonly }] = useBoolean(true);
  const titleId = useId('title');
  const [isCalloutVisible, { toggle: toggleIsCalloutVisible }] = useBoolean(false);
  const assigned = JSON.parse(props.metadata)
  const users = props.users
  let unmentioned = props.users//[]
  const labelId: string = useId('callout-label');
  const descriptionId: string = useId('callout-description');
  const [showFileName, setFileName] = React.useState("");
  const [commentId, setCommentId] = React.useState("");

  const [showCalendar, { toggle: toggleShowCalendar }] = useBoolean(false);
  const [selectedDate, setSelectedDate] = React.useState<Date>();

  const [typingChecklist, setTypingChecklist] = useState(props.typingChecklist)

  let textField: ITextField | undefined;
  const textFieldRef: IRefObject<ITextField> = (ref: ITextField | null) => {
    textField = ref!;
  };

  let duedateField: ITextField | undefined;
  const duedateRef: IRefObject<ITextField> = (ref: ITextField | null) => {
    duedateField = ref!;
  };

  let notesField: ITextField | undefined;
  const notesRef: IRefObject<ITextField> = (ref: ITextField | null) => {
    notesField = ref!;
  };

  let titleField: ITextField | undefined;
  const titleRef: IRefObject<ITextField> = (ref: ITextField | null) => {
    titleField = ref!;
  };

  let checklistLabel: ITextField | undefined;
  const checklistRef: IRefObject<ITextField> = (ref: ITextField | null) => {
    checklistLabel = ref!;
  };

  const attachmentRef = React.useRef(null);

  let unchecked = props.checklist.filter(item => item.isChecked == '0').length;
  let checked = props.checklist.filter(item => item.isChecked == '1').length;


  React.useEffect(() => {
    if (props.due_date)
      setSelectedDate(new Date(props.due_date + 'T00:00:00'));
    else
      setSelectedDate(undefined)
  }, [props.due_date])


  const linkRef = React.useRef();
  const [showContextualMenu, setShowContextualMenu] = React.useState(false);
  const onShowContextualMenu = React.useCallback(() => setShowContextualMenu(true), []);
  const onHideContextualMenu = React.useCallback(() => setShowContextualMenu(false), []);

  const [showAttachmentMenu, setShowAttachmentMenu] = React.useState(false);
  const onHideAttachmentMenu = React.useCallback(() => setShowAttachmentMenu(false), []);
  const [showCommentMenu, setShowCommentMenu] = React.useState(false);
  const onHideCommentMenu = React.useCallback(() => setShowCommentMenu(false), []);

  //move task phase
  const [hideDialog, { toggle: toggleHideDialog }] = useBoolean(true);
  const moveTaskId: string = useId('moveTaskId');
  const subTextId: string = useId('subTextLabel');
  const [selectedKeys, setSelectedKeys] = React.useState<string[]>(props.alert_recipients);

  const onChangeRecipient = (event: React.FormEvent<HTMLDivElement>, item?: IDropdownOption): void => {
    if (item) {
      setSelectedKeys(
        item.selected ? [...selectedKeys, item.key as string] : selectedKeys.filter(key => key !== item.key),
      );
    }
  };
  const [selectedItem, setSelectedItem] = React.useState<IDropdownOption>({ key: props.reminder_id, text: props.reminder_id });

  const onChangeAlert = (event: React.FormEvent<HTMLDivElement>, item?: IDropdownOption): void => {
    setSelectedItem((item as IDropdownOption<any>));
  };


  const modalProps = React.useMemo(
    () => ({
      titleAriaId: labelId,
      subtitleAriaId: subTextId,
      isBlocking: false,
      styles: dialogStyles,
    }),
    [moveTaskId, subTextId],
  );

  // const attachmentProps: IContextualMenuProps = {
  //   // For example: disable dismiss if shift key is held down while dismissing
  //   onDismiss: ev => {
  //     if (ev && ev.shiftKey) {
  //       ev.preventDefault();
  //     }
  //   },
  //   items: [
  //     {
  //       key: 'file',
  //       text: 'File',
  //       onClick: () => clickUploadBtnEvt()
  //     },
  //     {
  //       key: 'link',
  //       text: 'Link',
  //     },
  //   ],
  //   directionalHintFixed: true,
  // };

  //set reminder dialog
  const [hideReminderDialog, { toggle: toggleHideReminderDialog }] = useBoolean(true);
  const setReminderId: string = useId('setReminderId');
  const subTextReminderId: string = useId('subTextReminderLabel');

  const modalReminderProps = React.useMemo(
    () => ({
      titleAriaId: labelId,
      subtitleAriaId: subTextReminderId,
      isBlocking: false,
      styles: dialogStyles,
    }),
    [setReminderId, subTextReminderId],
  );

  const menuItems: IContextualMenuItem[] = [
    {
      key: 'move', onClick: () => toggleHideDialog(),
      iconProps: { iconName: 'Forward', style: { color: 'black' } }, text: 'Move task'
    },
    {
      key: 'alert', onClick: () => toggleHideReminderDialog(),
      iconProps: { iconName: 'AlarmClock', style: { color: 'black' } }, text: 'Set reminder'
    },
    {
      key: 'template', onClick: () => props.makeTemplate(props.card_id),
      iconProps: { iconName: 'Paste', style: { color: 'black' } }, text: props.is_template == 1 ? 'Remove template' : 'Make template'
    },
    {
      key: 'pin', onClick: () => pinTask(),
      iconProps: { iconName: props.status_qb_id === '' ? 'Unpin' : 'Pin', style: { color: 'black' } }, text: props.status_qb_id === '' ? 'Unpin the card' : 'Pin the card'
    },
    {
      key: 'addTimeActivity', onClick: () => addTimeActivity(),
      iconProps: { iconName: 'Assign', style: { color: 'black' } }, text: 'Add time activity'
    },
    {
      key: 'delete', onClick: () => deleteTask(),
      iconProps: { iconName: 'Delete', style: { color: 'black' } }, text: 'Delete'
    },
  ];

  const formatDate = (date) => {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

  const deleteTask = () => {
    let card = { id: props.card_id, active: 0 }
    props.saveCard(card)
    props.closePopup()
  }

  const pinTask = () => {
    let updateStatusId = props.status_qb_id === '' ? props.customer.status_qb_id : '-1'
    let card = { id: props.card_id, status_qb_id: updateStatusId }
    props.saveCard(card)
  }

  const addTimeActivity = () => {
    sessionStorage.setItem('addTimeActivityNote', JSON.stringify(({ note: props.description })))
    props.closePopup()
  }

  const onSelectDate = (date: Date): void => {
    toggleShowCalendar();
    setSelectedDate(new Date(formatDate(date) + 'T00:00:00'));
    //fire api to save due date
    //let card = { id: props.card_id, due_date: formatDate(date) }
    //props.saveCard(card)
  }

  //if due date textfield is edited without using calendar
  const changeDueDateTextField = (input) => {
    let re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

    if (input != '' && !input.match(re)) {
      //invalid date
      return false;
    }
    else {
      //if valid, fire api to save due date
      let card = { id: props.card_id, due_date: input == "" ? "" : formatDate(input) }
      props.saveCard(card)
      return true;
    }
  }

  const _getErrorMessage = (value: any) => {
    let re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

    return value != '' && !value.match(re) ? "Invalid date" : ""
  }

  const saveNotes = (description) => {
    let card = { id: props.card_id, description: description }
    props.saveCard(card)
  }

  const savePriority = (priority) => {
    let card = { id: props.card_id, priority: priority }
    props.saveCard(card)
  }

  const saveCategory = (lane_id) => {
    let card = { id: props.card_id, lane_id: lane_id }
    props.saveCard(card)
  }

  const onTitleBlur = () => {
    enableReadonly()
    let card = { id: props.card_id, title: (titleField as ITextField).value }
    if ((titleField as ITextField).value != props.title)
      props.saveCard(card)
  }

  const saveStatus = (status_qb_id) => {
    let card = { id: props.card_id, status_qb_id: status_qb_id }
    if (status_qb_id != props.status_qb_id) {
      props.saveCard(card)
      toggleHideDialog()
    }
  }

  const setReminder = (reminder_id, recipients) => {
    let card = { id: props.card_id, reminder_id: reminder_id }
    if (reminder_id != props.reminder_id || recipients != props.alert_recipients) {
      props.saveCard(card)
      props.saveRecipients({ card_id: props.card_id, recipients: recipients })
      toggleHideReminderDialog()
    }
  }

  const addChecklist = () => {
    let checklist = { card_id: props.card_id, label: (checklistLabel as ITextField ).value, isChecked: '0', mentions: [] }
    props.addChecklist(checklist)
    setTypingChecklist('')
  }

  //rearrange order after checklist drag
  const handleOnDragEnd = (result) => {
    if (!result.destination) return;
    const items = Array.from(props.checklist);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);
    props.arrangeChecklist(items, props.card_id, result.source.index, result.destination.index)
  }

  // const clickUploadBtnEvt = () => {
  //   attachmentRef.current.click()
  // }

  //upload attachment
  const selectFile = (event) => {
    console.log(event.target.files[0])
    const file = event.target.files[0]
    const attachment_url = `${GraphApi.itemUrl}/${GraphApi.attachmentId}/children`
    props.attachmentLoadingBegins()

    GraphApi.getToken().then(accessToken => {
      var reader = new FileReader();
      var fileByteArray
      reader.readAsArrayBuffer(file);
      reader.onloadend = function (evt) {
        let FileReaderResult = evt.target as FileReader

        if (FileReaderResult.readyState == FileReader.DONE) {
          fileByteArray = FileReaderResult.result

          GraphApi.getFile(accessToken, attachment_url).then(result => {
            let children = result.value
            //find out if folder has been created or not
            let child_folder = children.filter(child => child.name == props.card_id && child.folder)
            let res = child_folder.length
            let item = ""
            new Promise<void>((resolve, reject) => {
              if (res == 0) {
                GraphApi.createFolder(accessToken, attachment_url, props.card_id).then(result => {
                  item = JSON.parse(JSON.stringify(result)).id
                  resolve()
                })
              }
              else {
                item = child_folder[0].id
                resolve()
              }
            }).then(() => {
              const upload_url = `${GraphApi.itemUrl}/${item}:/${file.name}:/createUploadSession`
              createUploadSession(upload_url, accessToken).then(value => {
                uploadFile(value as string, file, fileByteArray).then(() => {
                  //add loading animation
                  props.addAttachment()
                  console.log('upload success')
                })
              })
            })
          })
        }
      }
    })
  }

  const createUploadSession = (url, accessToken) => {
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: 'PUT',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': accessToken
        }),
      })
        .then(res => {
          return res.json();
        })
        .catch(error => {
          console.dir('Error:', error)
          reject(error)
        })
        .then(response => {
          console.log('Success:', response)
          resolve(response.uploadUrl)
        });
    })
  }

  const uploadFile = (uploadUrl: string, file, fileByteArray) => {
    return new Promise((resolve, reject) => {
      fetch(uploadUrl, {
        method: 'PUT',
        headers: new Headers({
          'Content-Length': file.size,
          'Content-Range': `bytes 0-${file.size - 1}/${file.size}`,
        }),
        body: fileByteArray
      })
        .then(res => {
          return res.json();
        })
        .catch(error => {
          console.dir('Error:', error)
          reject(error)
        })
        .then(response => {
          console.log('Success:', response)
          resolve(response)
        });
    })
  }

  const openAttachmentMenu = (item_id) => {
    setShowAttachmentMenu(!showAttachmentMenu)
    setFileName(item_id)
  }

  const openCommentMenu = (comment_id) => {
    setShowCommentMenu(!showCommentMenu)
    setCommentId(comment_id)
  }

  const keypressHandler = event => {
    if (event.key === "Enter") {
      //console.log('enter!')
      addChecklist()
    }
  }
  
  const getCategory = () => {
      return props.boardLane.map(item => {
        return { key: item.id.toString(), text: item.title }
      })
  }

  const dropdownRef = React.createRef<IDropdown>();
  const dropdownReminderRef = React.createRef<IDropdown>();
  const dropdownRecipientsRef = React.createRef<IDropdown>();
  return (
    <div>
      <Modal
        titleAriaId={titleId}
        isOpen={props.opened}
        onDismiss={props.closePopup}
        isBlocking={false}
        containerClassName={contentStyles.container}
      >
        <div className={contentStyles.header}>
          <TextField componentRef={titleRef} maxLength={50}
            styles={{ field: { fontSize: '21px', fontWeight: '600', width: '550px' } }} borderless={isTextFieldBorderless}
            placeholder='Enter a task name' defaultValue={props.title} readOnly={isTextFieldReadonly}
            onClick={disableReadonly} onBlur={onTitleBlur} onMouseOver={showBorder} onMouseOut={hideBorder}
          />
          <IconButton
            className={'moreMenu'}
            styles={iconButtonStyles}
            iconProps={moreIcon}
            ariaLabel="More"
            onClick={() => setShowContextualMenu(!showContextualMenu)}
          />
          <ContextualMenu
            items={menuItems}
            hidden={!showContextualMenu}
            target={`.moreMenu`}
            onItemClick={onHideContextualMenu}
            onDismiss={onHideContextualMenu}
          />
          <IconButton
            iconProps={cancelIcon}
            ariaLabel="Close popup modal"
            onClick={props.closePopup}
          />
        </div>
        <div className={contentStyles.body}>
          <ActionButton iconProps={addFriendIcon} onClick={toggleIsCalloutVisible} className={'assignBtn'}>&nbsp;&nbsp;Assign&nbsp;&nbsp;
            {assigned && assigned.length > 0 ?
              <Stack horizontal tokens={{ childrenGap: 10 }}>
                {assigned.map(assigned_ => {
                  return <Persona
                    {...assigned_}
                    size={PersonaSize.size24}
                    presence={PersonaPresence.none}
                    hidePersonaDetails={props.hidePersona}
                  />
                })}
              </Stack>
              : null
            }
          </ActionButton>
        </div>
        {isCalloutVisible && (
          <Callout
            className={styles.callout}
            ariaLabelledBy={labelId}
            ariaDescribedBy={descriptionId}
            role="alertdialog"
            gapSpace={0}
            target={`.assignBtn`}
            onDismiss={toggleIsCalloutVisible}
            setInitialFocus
            isBeakVisible={false}
            directionalHint={DirectionalHint.bottomLeftEdge}
          >
            <div className={styles.body}>
              {/* <TextField label="" placeholder="Search members" />   */}
              <div className={styles.category}>
                <Text className={styles.title}>
                  Assigned
                </Text>
                <Stack padding={'10px 10px 10px 0px'} tokens={vStackTokens}>
                  {assigned.map(assigned_ => {
                    return <div className={'onHover'}><Stack horizontal horizontalAlign="space-between" verticalAlign="center">
                      <Persona
                        {...assigned_}
                        size={PersonaSize.size28}
                        presence={PersonaPresence.none}
                        hidePersonaDetails={false}
                        onClick={() => props.deleteMember(assigned_.text, assigned_.imageInitials)} />
                      <IconButton onClick={() => props.deleteMember(assigned_.text, assigned_.imageInitials)} iconProps={cancelIcon}></IconButton>
                    </Stack></div>
                  })}
                </Stack>
                <Text className={styles.title}>
                  Unassigned
                </Text>
                <Stack padding={'10px 10px 10px 0px'} tokens={vStackTokens}>
                  {users.map(users_ => {
                    return <div className={'onHover'}><Persona
                      {...users_}
                      size={PersonaSize.size28}
                      presence={PersonaPresence.none}
                      hidePersonaDetails={false}
                      onClick={() => props.addMember(users_.text, users_.imageInitials)} /></div>
                  })}
                </Stack>
              </div>
            </div>
          </Callout>
        )}
        <div className={contentStyles.body}>
          <Stack horizontal tokens={stackTokens} styles={stackStyles}>
            <Stack {...columnProps}>
              <TextField label="Client" value={props.customerName} readOnly />
            </Stack>
            {/* <Stack {...columnProps}>
              <TextField label="Client status" value={props.customerStatus} readOnly />
            </Stack> */}

          </Stack>
          {/* Second Row */}
          <Stack horizontal tokens={stackTokens} styles={stackStyles}>
            <Stack {...columnProps}>
              <Dropdown
                label="Category"
                options={getCategory()}
                defaultSelectedKey={props.lane_id}
                onChange={(e, selectedOption) => {
                  saveCategory((selectedOption as IDropdownOption<any>).key)
                }}
              />
            </Stack>
            <Stack {...columnProps}>
              <Dropdown
                label="Priority"
                onRenderOption={onRenderOption}
                onRenderTitle={onRenderTitle}
                options={priorityOptions}
                defaultSelectedKey={props.priority}
                onChange={(e, selectedOption) => {
                  savePriority(selectedOption.key)
                }}
              />
            </Stack>
            <Stack {...columnProps}>
              <div ref={calendarBtn => (calendarButtonElement = calendarBtn!)}>
                <TextField
                  componentRef={duedateRef}
                  onBlur={() => changeDueDateTextField((duedateField as ITextField ).value)}
                  label="Due date"
                  value={!selectedDate ? "" : selectedDate.toLocaleDateString()}
                  iconProps={iconProps}
                  onClick={toggleShowCalendar}
                  placeholder="Due anytime"
                  onGetErrorMessage={() => _getErrorMessage(duedateField ? duedateField.value : "")}
                />
              </div>
              {showCalendar && (
                <Callout
                  isBeakVisible={false}
                  className="ms-DatePicker-callout"
                  gapSpace={0}
                  doNotLayer={false}
                  target={calendarButtonElement}
                  directionalHint={DirectionalHint.bottomLeftEdge}
                  onDismiss={toggleShowCalendar}
                  setInitialFocus
                >
                  <FocusTrapZone firstFocusableSelector="ms-DatePicker-day--today" isClickableOutsideFocusTrap>
                    <Calendar
                      // eslint-disable-next-line react/jsx-no-bind
                      onSelectDate={onSelectDate}
                      onDismiss={toggleShowCalendar}
                      isMonthPickerVisible={true}
                      value={selectedDate!}
                      firstDayOfWeek={DayOfWeek.Sunday}
                      strings={DayPickerStrings}
                      isDayPickerVisible={true}
                      showMonthPickerAsOverlay={true}
                      showGoToToday={false}
                    />
                  </FocusTrapZone>
                </Callout>
              )}
            </Stack>
          </Stack>

          <Stack horizontal tokens={stackTokens} styles={stackStyles}>
            <Stack {...columnProps}>
              <TextField label="Notes" maxLength={200} multiline resizable={true} defaultValue={props.description} onBlur={() => { saveNotes((notesField as ITextField ).value) }} componentRef={notesRef} />
            </Stack>
          </Stack>
        </div>
        <div className={contentStyles.body}>
          <Label>
            Checklist&nbsp;&nbsp;
            {unchecked + checked > 0 &&
              <span>{checked} / {unchecked + checked}</span>
            }
          </Label>
          {unchecked + checked > 0 &&
            <ProgressIndicator percentComplete={(checked) / (unchecked + checked)} />
          }
          {/* Editable checklist */}
          <DragDropContext onDragEnd={handleOnDragEnd}>
            <Droppable droppableId="clist">
              {(provided) => (<div className='clist' {...provided.droppableProps} ref={provided.innerRef}>
                {props.checklist.map((item, index) => {
                  if (assigned) {
                    unmentioned = users.filter(obj1 => item.mentions.every(obj2 => obj1.text !== obj2.text))
                    return (
                      <Draggable key={'list' + index} draggableId={'list' + index} index={index}>
                        {(provided) => (
                          <Checklist innerRef={provided.innerRef} provided={provided}
                            item={item}
                            index={index}
                            card_id={props.card_id}
                            unmentioned={unmentioned}
                            editChecklist={props.editChecklist}
                            addMention={props.addMention}
                            deleteMention={props.deleteMention}
                            deleteChecklist={props.deleteChecklist}
                          />
                        )}
                      </Draggable>
                    );
                  } else {
                    return <div></div>
                  }

                })}
                {provided.placeholder}
              </div>
              )}
            </Droppable>
          </DragDropContext>
          {/* New checklist textfield */}
          <Stack horizontal verticalAlign="center" style={{ marginLeft: '15px' }}>
            <Checkbox styles={checkboxStyles} disabled />
            <TextField styles={{ field: { width: '550px', paddingLeft: 2 } }} componentRef={checklistRef}
              value={typingChecklist} onBlur={addChecklist} placeholder="Add an item" borderless={true}
              onKeyPress={event => keypressHandler(event)} onChange={(e)=>{setTypingChecklist((e.target as any).value)}} 
            />
          </Stack>
        </div>

        <div className={contentStyles.commentSection}>
          <TextField label="Comments" value={props.typingComment} onChange={props.changed} componentRef={textFieldRef}
            styles={{ subComponentStyles: { label: { root: { fontWeight: 640 } } } }}
            multiline resizable={false} placeholder="Type your message here" />
        </div>
        <div className={contentStyles.commentBtn}>
          <DefaultButton text="Send" disabled={props.addCommentBtnDisabled} onClick={() => { props.addComment((textField as ITextField).value); }} />
        </div>

        {props.comments.map(comments_ => {
          return <Comment key={comments_.id} {...comments_} openMenu={openCommentMenu} />
        })}
        <ContextualMenu
          items={[
            {
              key: 'remove', onClick: () => { console.log(commentId); props.deleteComment(commentId) },
              iconProps: { iconName: 'Delete', style: { color: 'black' } }, text: 'Remove'
            },
          ]}
          hidden={!showCommentMenu}
          target={`.comment-more`}
          onItemClick={onHideCommentMenu}
          onDismiss={onHideCommentMenu}
        />
      </Modal>

      <Dialog
        hidden={hideDialog}
        onDismiss={toggleHideDialog}
        dialogContentProps={dialogContentProps}
        modalProps={modalProps}
        minWidth='500px'
      >
        <Dropdown
          label=''
          options={props.phases}
          componentRef={dropdownRef}
          dropdownWidth={0}
          placeholder='Select a client phase'
          defaultSelectedKey={props.status_qb_id}
        />

        <DialogFooter>
          <DefaultButton onClick={() => saveStatus((dropdownRef.current as IDropdown).selectedOptions[0] ? (dropdownRef.current as IDropdown).selectedOptions[0].key : '')} text="Save" />
          <DefaultButton onClick={toggleHideDialog} text="Close" />
        </DialogFooter>
      </Dialog>

      <Dialog
        hidden={hideReminderDialog}
        onDismiss={toggleHideReminderDialog}
        dialogContentProps={dialogReminderProps}
        modalProps={modalReminderProps}
        minWidth='500px'
      >
        <Dropdown
          label='Repeat'
          style={{ marginBottom: '15px' }}
          options={[{ key: '1', text: 'Every day' },
          { key: '2', text: 'Every 2 days' },
          { key: '3', text: 'Every 3 days' },
          { key: '4', text: 'Every 4 days' },
          { key: '5', text: 'Every 5 days' },
          { key: '6', text: 'Every 6 days' },
          { key: '7', text: 'Every 7 days' },
          { key: '14', text: 'Every 14 days' },
          { key: '30', text: 'Every 30 days' },
          { key: '45', text: 'Every 45 days' },
          { key: '60', text: 'Every 60 days' },
          { key: '90', text: 'Every 90 days' },
          { key: '120', text: 'Every 120 days' },
          { key: '150', text: 'Every 150 days' },
          { key: '180', text: 'Every 180 days' },
          { key: '360', text: 'Every 360 days' },
          ]}
          componentRef={dropdownReminderRef}
          dropdownWidth={0}
          placeholder='Never'
          selectedKey={selectedItem ? selectedItem.key : undefined}
          onChange={onChangeAlert}
        />
        <Dropdown
          placeholder="Select recipients"
          label="Recipients"
          selectedKeys={selectedKeys}
          onChange={onChangeRecipient}
          multiSelect
          options={assigned}
          componentRef={dropdownRecipientsRef}
        />

        <DialogFooter>
          <DefaultButton onClick={() => setReminder((dropdownReminderRef.current as IDropdown).selectedOptions[0] ? (dropdownReminderRef.current as IDropdown).selectedOptions[0].key : '', (dropdownReminderRef.current as IDropdown).selectedOptions)} text="Save" />
          <DefaultButton onClick={toggleHideReminderDialog} text="Close" />
        </DialogFooter>
      </Dialog>
    </div>
  );
};

//Style
const contentStyles = mergeStyleSets({
  container: {
    display: 'flex',
    width: '700px',
    height: '650px',
    flexFlow: 'column nowrap',
    alignItems: 'stretch',
    overflowY: 'auto',
    padding: '0px 0px 24px 0px'
  },
  header: [
    // eslint-disable-next-line deprecation/deprecation
    theme.fonts.xLargePlus,
    {
      flex: '1 1 auto',
      borderTop: `4px solid ${theme.palette.themePrimary}`,
      color: theme.palette.neutralPrimary,
      display: 'flex',
      alignItems: 'center',
      fontWeight: FontWeights.semibold,
      padding: '12px 12px 14px 24px',
    },
  ],
  body: {
    flex: '4 4 auto',
    padding: '0 24px 24px 24px',
    overflowY: 'hidden',
    selectors: {
      p: { margin: '14px 0' },
      'p:first-child': { marginTop: 0 },
      'p:last-child': { marginBottom: 0 },
    },
  },
  commentSection: {
    flex: '4 4 auto',
    padding: '0 24px 10px 24px',
    overflowY: 'hidden',
  },
  commentBtn: {
    flex: '4 4 auto',
    padding: '0 24px 40px 24px',
    overflowY: 'hidden',
    textAlign: 'right'
  },
});
const styles = mergeStyleSets({
  callout: {
    maxWidth: 300,
    width: 288,
  },
  body: {
    padding: '18px 24px 12px',
  },
  category: {
    padding: '10px 0px 10px 0px',
    height: '300px'
  },
  title: [
    theme.fonts.large,
    {
      margin: 0,
      fontWeight: FontWeights.semilight,
    },
  ],
  subtext: [
    theme.fonts.small,
    {
      margin: 0,
      fontWeight: FontWeights.semilight,
    },
  ]
});
const checkboxStyles = { checkbox: { borderRadius: '50%' } };
const iconButtonStyles = {
  root: {
    color: theme.palette.neutralPrimary,
    marginLeft: 'auto',
    marginTop: '4px',
    marginRight: '2px',
  },
  rootHovered: {
    color: theme.palette.neutralDark,
  },
};
