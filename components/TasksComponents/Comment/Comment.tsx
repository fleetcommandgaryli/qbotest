import * as React from 'react';
import { getTheme, mergeStyleSets, IconButton, Persona, PersonaSize } from '@fluentui/react';

const theme = getTheme();
const comment = (props) => {
    return <div className={contentStyles.commentSection}>
        <div >
            <div>
                <Persona imageInitials={props.initials} text={props.name} size={PersonaSize.size24} />
            </div>
            <div>
                {props.date}
            </div>
        </div>
        <div >
            <div className={contentStyles.comment}>
                {props.content}
            </div>
            <div>
                <IconButton
                    className={'comment-more'}
                    styles={iconButtonStyles}
                    iconProps={{ iconName: 'More' }}
                    onClick={() => props.openMenu(props.id)}
                />
            </div>
        </div>
    </div>
};
const contentStyles = mergeStyleSets({
    commentSection: {
        flex: '4 4 auto',
        padding: '0 24px 10px 24px',
        overflowY: 'hidden',
    },
    commentBtn: {
        flex: '4 4 auto',
        padding: '0 24px 40px 24px',
        overflowY: 'hidden',
        textAlign: 'right'
    },
    comment: {
        padding: '0 0 0 36px',
        color: 'black'
    }
});
const iconButtonStyles = {
    root: {
        color: theme.palette.neutralPrimary,
        marginLeft: 'auto',
        marginTop: '4px',
        marginRight: '2px',
    },
    rootHovered: {
        color: theme.palette.neutralDark,
    },
};

export default comment;