import React from 'react';
import Board from 'react-trello';
import { TextField } from '@fluentui/react';
import { PrimaryButton } from '@fluentui/react';
import { Dropdown, IDropdownOption, IDropdown } from '@fluentui/react';
import { QBOTaskApi } from '../../../api/QBOTaskApi';
import { QuickbooksApi } from '../../../api/QuickbooksApi';

class NewCardForm extends React.Component<Board, {}> {
    handleAdd = () => this.props.onAdd({
        title: this.titleRef.value,
        template: this.dropdownRef.current.selectedOptions[0] ? this.dropdownRef.current.selectedOptions[0].key : '0',
        checklist: [], comment: [], label: null,
        customer: this.customerDropdownRef.current.selectedOptions[0] ? this.customerDropdownRef.current.selectedOptions[0].key : '0'
    })
    titleRef: any;
    setTitleRef = (ref) => this.titleRef = ref

    dropdownRef = React.createRef<IDropdown>();
    customerDropdownRef = React.createRef<IDropdown>();

    

    constructor(props) {
        super(props)
    }

    render() {
        const { onCancel } = this.props

        let options_: { key: any; text: any; }[] = [];
        let customerOption: { key: any; text: any; }[] = [];
        const getAllCustomer = QuickbooksApi.getAllCustomer()
        const getTemplate = QBOTaskApi.getTemplates()

        Promise.all([getTemplate, getAllCustomer ]).then(response =>{
            console.dir(response[0])
            console.dir(response[1])

            let tmplate = response[0]
            if(tmplate){
                tmplate.forEach(item => {
                    options_.push({ key: item.id, text: item.title })
                })
            }

            let customers = response[1].QueryResponse.Customer
            if(customers){
                customers.forEach(customer => {
                    customerOption.push({ key: customer.Id, text: customer.PrintOnCheckName })
                })
            }
            
        })

        
        // QBOTaskApi.getTemplates().then(response => {
        //     let i = 0;
        //     while (response[i]) {
        //         options_.push({ key: response[i].id, text: response[i].title })
        //         i++
        //     }

        // })

        const options: IDropdownOption[] = options_

        return (
            <div style={{ background: 'white', borderRadius: 3, border: '1px solid #eee', borderBottom: '1px solid #ccc' }}>
                <div style={{ padding: 5, margin: 5 }}>
                    <div>
                        <div style={{ marginBottom: 5 }}>
                            <TextField componentRef={this.setTitleRef} placeholder="Enter a task name" />
                        </div>
                    </div>
                    <div style={{ marginBottom: 5 }}>
                        <Dropdown
                            placeholder="Select a template"
                            options={options}
                            componentRef={this.dropdownRef}
                        />
                    </div>
                    <div style={{ marginBottom: 5 }}>
                        <Dropdown
                            placeholder="Select a customer"
                            options={customerOption}
                            componentRef={this.customerDropdownRef}
                        />
                    </div>
                    <div style={{ marginBottom: 5 }}>
                        <PrimaryButton text="Add Task" onClick={this.handleAdd} style={{ width: '100%', background: '#137ad1' }} />
                    </div>
                    <div style={{ marginBottom: 5 }}>
                        <PrimaryButton text="Cancel" onClick={onCancel} style={{ width: '100%', background: '#e3e3e3', color: '#797979' }} />
                    </div>
                </div>
            </div>
        )
    }
}
export default NewCardForm