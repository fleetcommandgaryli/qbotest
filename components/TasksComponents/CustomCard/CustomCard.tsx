import React from 'react';
import { MovableCardWrapper } from 'react-trello/dist/styles/Base'
import { Stack } from '@fluentui/react';
import { Persona, PersonaSize, PersonaPresence } from '@fluentui/react';
import 'office-ui-fabric-core/dist/css/fabric.min.css'

const CustomCard = ({
  onClick, className, cardStyle, description, label, priority, cardColor, metadata, onDelete, title, is_template, attachments_count, checklist_checked_count, checklist_count, comments_count, status_qb_id
}) => {
  const clickDelete = e => {
    onDelete()
    e.stopPropagation()
  }

  let color = '#605E5C'
  let backgroundColor = 'transparent'
  if (new Date(label) < new Date()) {
    color = 'white'
    backgroundColor = '#D13438'
  }

  return (
    <MovableCardWrapper
      onClick={onClick}
      style={cardStyle}
      className={className}
    >
      <header
        style={{
          borderBottom: '0px solid #eee',
          whiteSpace: 'pre-wrap',
          paddingBottom: 6,
          marginBottom: 10,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          color: cardColor
        }}>
        <div style={{ fontSize: 14, fontWeight: 'bold' }}>{title}</div>
      </header>
      <div style={{ color: '#605E5C' }}>
        <div style={{ fontSize: 12, whiteSpace: 'pre-wrap' }}>
          {description}
        </div>

        {status_qb_id == '' || is_template == '1' || priority == '1' || priority == '2' || label || parseInt(checklist_count) > 0 || parseInt(comments_count) > 0 || parseInt(attachments_count) > 0 ?
          <div style={{ marginTop: '20px', marginBottom: '10px', fontSize: 12, display: 'flex', flexWrap: 'wrap' }}>
            {status_qb_id == '' &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px', color: '#182c4c', backgroundColor: '#e5f0f6' }} aria-label="template" title="template">
                <i className="ms-Icon ms-Icon--Pinned"></i>&nbsp;&nbsp;
                <span tabIndex={-1}>Pinned</span>
              </div>
            }
            {is_template == '1' &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px', color: '#182c4c', backgroundColor: '#e5f0f6' }} aria-label="template" title="template">
                <i className="ms-Icon ms-Icon--Paste"></i>&nbsp;&nbsp;
                <span tabIndex={-1}>Template</span>
              </div>
            }
            {priority == '1' &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px', color: '#CF363D' }}>
                <i className="ms-Icon ms-Icon--Ringer" tabIndex={-1} aria-label="Urgent" title="Urgent"></i>
              </div>
            }
            {priority == '2' &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px', color: '#CF363D' }}>
                <i className="ms-Icon ms-Icon--Important" tabIndex={-1} aria-label="Important" title="Important"></i>
              </div>
            }
            {label &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px', color: color, backgroundColor: backgroundColor }} aria-label="due date" title="due date">
                <i className="ms-Icon ms-Icon--CalendarDay" aria-hidden="true"></i>&nbsp;&nbsp;
                <span tabIndex={-1}>{new Date().getFullYear() == label.substring(6) ? label.substring(0, 5) : label}</span>
              </div>
            }
            {parseInt(comments_count) > 0 &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px' }} aria-label="comments" title="comments">
                <i className="ms-Icon ms-Icon--Chat"></i>&nbsp;&nbsp;
                {comments_count}
              </div>
            }
            {parseInt(checklist_count) > 0 &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px' }} aria-label="checklist" title="checklist">
                <i className="ms-Icon ms-Icon--Completed"></i>&nbsp;&nbsp;
                {checklist_checked_count}/{checklist_count}
              </div>
            }
            {parseInt(attachments_count) > 0 &&
              <div style={{ borderRadius: 3, padding: '3px', display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px' }} aria-label="attachments" title="attachments">
                <i className="ms-Icon ms-Icon--Attach"></i>&nbsp;&nbsp;
                {attachments_count}
              </div>
            }
          </div> : null
        }

        {metadata && metadata.length > 0 ?
          <div style={{ borderTop: '1px solid #eee', margin: '0px -10px 0 -10px', paddingTop: 6 }} >
            <Stack horizontal style={{ paddingLeft: 10 }} tokens={{ childrenGap: '10 12' }} wrap>
              {metadata.map((assigned_, index) => {
                return <div key={index}><Persona
                  {...assigned_}
                  size={PersonaSize.size28}
                  presence={PersonaPresence.none}
                  presenceTitle={assigned_.title}
                  hidePersonaDetails={metadata.length > 1 ? true : false}
                /></div>
              })}
            </Stack>
          </div>
          : null
        }
      </div>
    </MovableCardWrapper>
  )
}
export default CustomCard;