import React, {useState} from 'react';
import { useBoolean, useId } from '@fluentui/react-hooks';
import { TextField, ITextField } from '@fluentui/react';
import { IRefObject } from '@fluentui/react';
import { getTheme, IconButton, FontWeights, Callout, DirectionalHint, mergeStyleSets, IIconProps } from '@fluentui/react';
import { Stack, IStackTokens } from '@fluentui/react';
import { Text } from '@fluentui/react';
import { Persona, PersonaSize, PersonaPresence } from '@fluentui/react';
import { Checkbox } from '@fluentui/react';
import { TooltipHost } from '@fluentui/react';
// import './Checklist.css'
require('office-ui-fabric-core/dist/css/fabric.min.css')

const theme = getTheme();
let checklistLabel: ITextField | undefined;
const checklistRef: IRefObject<ITextField> = (ref: ITextField | null) => {
  checklistLabel = ref!;
};

const checklistRefs = [];

const Checklist = ({item, index, card_id, unmentioned, editChecklist, deleteMention, addMention, deleteChecklist, innerRef, provided}) => {    

  const [isMentionVisible, { toggle: toggleMentionVisible }] = useBoolean(false);
  const [iconDisplay, setIconDisplay] = useState({});
  const labelId: string = useId('callout-label');
  const descriptionId: string = useId('callout-description');

  const hoverChecklist = index =>
  {
    setIconDisplay(prevState => ({
      ...prevState,
      [index]: true
    }));    
  }

  const outChecklist = index =>
  {
    setIconDisplay(prevState => ({
      ...prevState,
      [index]: false
    }));    
  }

  return (
    <div className='checklist' ref={innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
    <Stack styles={{root: {background: iconDisplay[index] ? '#f3f2f1' : 'transparent'}}} horizontal verticalAlign="center" onMouseOver={() => hoverChecklist(index)} onMouseLeave={() => outChecklist(index)} >    
    {iconDisplay[index] ? 
    <div className='dragHandle' />
    :
    <div style={{width: '15px'}} />
    }
    <Checkbox styles={checkboxStyles} onChange={() => editChecklist(null, item.id, 1)} checked={item.isChecked == '1' ? true : false} />
    <TextField styles={{field: {width: '530px', backgroundColor: iconDisplay[index] ? '#f3f2f1' : 'transparent', paddingLeft: 2, textDecoration: item.isChecked == '1' ? 'line-through' : 'none'}}}
      defaultValue={item.label} placeholder="Add an item" borderless={true} onBlur={() => editChecklist((checklistRefs[index] as any).value, item.id, null)} componentRef={ref => (checklistRefs[index] as any) = ref}
    />
    {iconDisplay[index] ? 
    <Stack horizontal>
      <TooltipHost content="Mention">
        <IconButton className={'mention'+index} menuIconProps={{ iconName:"Accounts" }} onClick={toggleMentionVisible} />
        {isMentionVisible && (
        <Callout
          className={styles.callout}
          ariaLabelledBy={labelId}
          ariaDescribedBy={descriptionId}
          role="alertdialog"
          gapSpace={0}
          target={`.mention${index}`}
          onDismiss={toggleMentionVisible}
          setInitialFocus
          isBeakVisible={false}          
          directionalHint={DirectionalHint.bottomLeftEdge}
        >          
          <div className={styles.body}>
            <div className={styles.category}>
                <Text className={styles.title}>
                Mentioned                
                </Text>
                <Stack padding={'10px 10px 10px 0px'} tokens={vStackTokens}>
                {item.mentions.map(mentioned => {
                    return <div className={'onHover'}><Stack horizontal horizontalAlign="space-between" verticalAlign="center">                        
                        <Persona
                        {...mentioned}
                        size={PersonaSize.size28}
                        presence={PersonaPresence.none}
                        hidePersonaDetails={false}
                        onClick={() => deleteMention(card_id, mentioned.id)} 
                        />
                        <IconButton onClick={() => deleteMention(card_id, mentioned.id)} iconProps={cancelIcon}></IconButton>
                        </Stack></div>
                })}
                </Stack>
                <Text className={styles.title}>
                Assigned
                </Text>
                <Stack padding={'10px 10px 10px 0px'} tokens={vStackTokens}>
                {unmentioned.map(user => {
                    return <div className={'onHover'}><Persona
                        {...user}
                        size={PersonaSize.size28}
                        presence={PersonaPresence.none}
                        hidePersonaDetails={false}                        
                        onClick={() => addMention(card_id, item.id, user.text)} 
                        />
                        </div>
                })}
                </Stack>
            </div>                              
          </div>
        </Callout>
        )}
      </TooltipHost>
      <TooltipHost content="Delete">
        <IconButton iconProps={{iconName: 'Delete'}} onClick={() => deleteChecklist(item.id)} />
      </TooltipHost>
    </Stack>              
    : null
    }
  </Stack>
  {item.mentions.length > 0 ?
  <div style={{paddingLeft:'24px'}}>
    <Stack horizontal tokens={{childrenGap: 10}}>
    {item.mentions.map(mention => {
      return <Persona size={PersonaSize.size24} hidePersonaDetails={false} text={mention.text} imageInitials={mention.imageInitials} />
    })}                
    </Stack>
  </div>
  : null
  }
  </div>

  )  
}

const styles = mergeStyleSets({    
  callout: {
    maxWidth: 300,
    width: 288,
  },
  body: {
    padding: '18px 24px 12px',
  },
  category: {
    padding: '10px 0px 10px 0px',
    height: '300px'
  },
  title: [
    theme.fonts.large,
    {
      margin: 0,
      fontWeight: FontWeights.semilight,
    },
  ],    
  subtext: [
    theme.fonts.small,
    {
      margin: 0,
      fontWeight: FontWeights.semilight,
    },
  ]
});
const checkboxStyles = { checkbox: { borderRadius: '50%' } };
const vStackTokens: IStackTokens = { childrenGap: 10 };
const cancelIcon: IIconProps = { iconName: 'Cancel' };

export default Checklist