import React, { Fragment } from 'react';
import Board from 'react-trello';
import { IQboKanbanProps, IQboKanbanStatus } from '../../components/KanbanComponents/IQboKanbanProps';
import testdata from "./data/data.json";
import { QuickbooksApi } from '../../api/QuickbooksApi';
import { QBOTaskApi } from '../../api/QBOTaskApi';
import { Setting } from '../../utils/Setting';
import { CustomHeader } from '../../components/KanbanComponents/CustomLaneHeader'
import CustomCard from '../../components/KanbanComponents/CustomCard'
import styled from 'styled-components'
import { KanbanModal } from '../../components/KanbanComponents/Modal'
import { ImportModal } from '../../components/KanbanComponents/ImportModal'
import { GraphApi } from '../../api/GraphApi';
import { Loading } from '../../utils/notify/notifyMessage';
import { Dropdown, DefaultButton } from '@fluentui/react';
import Autocomplete from '../../components/Autocomplete/Autocomplete';
import moment from 'moment';
import { RefreshToken } from '../../interfaces/IQboCustomerProps';
import { TimerModal } from '../../components/TimerModal/TimerModal';
import Layout from '../../components/Layout';
import { useSelector } from 'react-redux'
import store, { storeType } from '../../reducers/configueStore'
import { setToken } from '../../reducers/token'
import axios from 'axios';
import { initializeIcons } from '@fluentui/react/lib-commonjs/Icons'
import { LoginCheck, LogOut } from '../../utils/LoginUtil';
const openNewTab = Setting.clientPage

export default class QboKanban extends React.Component<IQboKanbanProps, IQboKanbanStatus> {

  constructor(props: IQboKanbanProps) {
    super(props);
    initializeIcons()
    this.state = {
      accessToken: '',
      resourceData: testdata,
      data: testdata,
      customers: [],
      selectCustomer: {},
      modalOpen: false,
      modalData: [],
      taskID: null,
      statusChangeId: '',
      role: [],
      getRoleFinished: false,
      filterStatusList: [],
      filterEmployeeList: [],
      filterUrgent: [],
      taskStatus: [],
      initLoading: false,
      employee: [],
      employeeChange: '',
      allSharePointUsers: [],
      searchCustomer: '',
      isTimerModalOpen: false,
      importModalOpen: false,
      customerBoardLane: [],
      boardLane: [],
    };
    this.updateCustomer = this.updateCustomer.bind(this)
    this.TimerModalClose = this.TimerModalClose.bind(this)
    this.refreshToken = this.refreshToken.bind(this)
    this.tokenTimer = this.tokenTimer.bind(this)
    this.reloadData = this.reloadData.bind(this)
  }

  async componentDidMount() {
    const isLogin = await LoginCheck()
    console.dir(isLogin)
    if (isLogin) {
      try {
        await this.reloadData()
      } catch (error) {
        console.dir(error)
        // LogOut()
      }
    }
  }


  public render(): React.ReactElement<IQboKanbanProps> {
    return (
      <Fragment>
        <div style={{ paddingTop: "10px" }}>
          <div>
            <div style={{ margin: '0 12px 0 12px', display: 'flex', justifyContent: 'space-between' }}>
              <Autocomplete
                suggestions={
                  this.state.customers.filter(customer => {
                    return parseInt(customer.Level) >= 2
                  }).map(customer => {
                    return { key: customer.Id, text: customer.DisplayName }
                  })
                }
                onSearch={(item) => {
                  item = typeof (item) === 'object'
                    ? item.text
                    : item

                  this.setState({ searchCustomer: item }, () => {
                    console.log(item)
                    this.filterChangeHandler(null, null)
                  })
                }}
                placeholder="Search customer"
                width={570}
                userInput={this.state.searchCustomer}
              />
              <DefaultButton text="Import customers" onClick={this.importModalShow} />
            </div>

            {
              !this.state.initLoading ? <Loading></Loading> :
                <div>
                  <Board
                    style={{ backgroundColor: 'white', height: 'auto' }}
                    data={this.state.data}
                    laneStyle={{ width: '200px' }}
                    handleDragEnd={this.dragCardEnd}
                    components={{
                      Card: CustomCard,
                      ScrollableLane: ScrollableLane
                    }}
                    onCardClick={this.cardClick}
                    //editable
                    //canAddLanes
                    editLaneTitle
                    onLaneUpdate={(laneId, data) => this.updateTitle(laneId, data)}
                  />
                  <KanbanModal
                    modalHide={this.modalHide}
                    modalShow={this.modalShow}
                    modalOpend={this.state.modalOpen}
                    modalData={this.state.modalData}
                    customer={this.state.selectCustomer}
                    customers={this.state.customers}
                    openNewTab={this.openTab}
                    setTaskId={this.setTaskId}
                    dropDownOption={getOption(this.state.resourceData.lanes)}
                    changeStatusId={this.state.statusChangeId}
                    selectChangeStatusId={this.selectChangeStatusId}
                    updateCustomer={this.updateCustomer}
                    getStatusChangeId={this.state.statusChangeId}
                    employeeOption={this.getEmployeeOption()}
                    employeeChange={this.state.employeeChange}
                    selectChangeEmployee={this.selectChangeEmployee}
                    getEmployeeKey={this.getEmployeeKey}
                    boardLane={this.state.boardLane}
                  ></KanbanModal>
                  <ImportModal
                    modalOpend={this.state.importModalOpen}
                    modalHide={this.importModalHide}
                    boardData={this.state.data}
                    customers={this.state.customers}
                    reloadData={this.reloadData}
                  >
                  </ImportModal>
                </div>
            }
          </div>
          {/* <TimerModal
            isModalOpen={this.state.isTimerModalOpen}
            modalClose={this.TimerModalClose}
            refreshToken={this.refreshToken}
          ></TimerModal> */}
        </div>
      </Fragment>
    );
  }

  private filterChangeHandler(event, option) {
    console.dir(event)
    console.dir(option)

    let filterStatusList = JSON.parse(JSON.stringify(this.state.filterStatusList));
    let filterEmployeeList = JSON.parse(JSON.stringify(this.state.filterEmployeeList));
    let filterUrgent = JSON.parse(JSON.stringify(this.state.filterUrgent));

    if (event != null) {
      const elementId = event.target.id
      console.dir(elementId)
      if (elementId == 'statusFilter') {
        filterStatusList = updateFilterStatus(filterStatusList, option);
      }
      if (elementId == 'employeeFilter') {
        filterEmployeeList = updateFilterStatus(filterEmployeeList, option)
      }
      if (elementId == 'urgentFilter') {
        filterUrgent = updateFilterStatus(filterUrgent, option)
      }
    }

    this.setState({ filterStatusList: filterStatusList, filterEmployeeList: filterEmployeeList, filterUrgent: filterUrgent }, () => {
      let filterData = this.filterDate(this.state.resourceData, filterStatusList, filterEmployeeList, filterUrgent);
      this.setState({ data: filterData });
    });


    function updateFilterStatus(filterStatusList: any, option: any) {
      // console.dir(filterStatusList)
      // console.dir(option)
      let filterByPoperty = option.hasOwnProperty('display') ? 'display' : 'key'

      if (filterStatusList.indexOf(option[filterByPoperty].toString()) >= 0) {
        filterStatusList = filterStatusList.filter(item => {
          return item != option[filterByPoperty].toString();
        });
      } else {
        filterStatusList.push(option[filterByPoperty].toString());
      }

      return filterStatusList;
    }
  }



  reloadData() {
    // const getCustomerType = QuickbooksApi.getEntity('CustomerType', this.state.accessToken)
    const getAllCustomer = QuickbooksApi.getAllCustomer()
    const getTaskStatus = QBOTaskApi.getTaskStatus()
    const getEmployees = QuickbooksApi.getEntity('employee', '')

    axios.defaults.headers.common = { 'Authorization': `Bearer ${localStorage.getItem('token')}}` }
    const getCustomerBoardLaneData = axios.get('/Customer/Board')

    return Promise.all([getAllCustomer, getTaskStatus, getEmployees, getCustomerBoardLaneData]).then(result => {
      const customers = result[0].QueryResponse.Customer ?? []
      const taskStatus = result[1].data
      const employee = result[2].Result

      console.dir(result[3].data)
      const customerBoardLaneData: any = result[3].data

      const customerBoardLane = customerBoardLaneData.data
      const boardLane = customerBoardLaneData.boardLane

      console.dir(customers)
      console.dir(taskStatus)
      console.dir(customerBoardLane)

      let customersSortByName = customersSort(customers)

      this.setState({
        employee: employee,
        customers: customersSortByName,
        taskStatus: taskStatus,
        customerBoardLane: customerBoardLane,
        boardLane: boardLane
      }, () => {
        let boardObj = getLanes(customersSortByName, taskStatus, this.filterEmployeeOption(), customerBoardLane)
        let filterData = this.filterDate(boardObj, this.state.filterStatusList, this.state.filterEmployeeList, this.state.filterUrgent);

        this.setState({
          resourceData: boardObj,
          data: filterData,
          initLoading: true,
        })
      })
    })
  }

  dragCardEnd = (cardId, sourceLaneId, targetLaneId, position, cardDetails) => {
    console.dir(cardId)
    console.dir(sourceLaneId)
    console.dir(targetLaneId)
    console.dir(position)
    console.dir(cardDetails)

    let card = { id: cardDetails.cardId, lane_id: targetLaneId, position: position, customer_qb_id: cardId }
    this.saveCard(card)


    //fire api and save the new card position
    if (sourceLaneId != targetLaneId) {
      let customer = cardDetails.detail

      // console.dir(targetLaneId)
      // console.dir(customer)

      //customer = this.updateCustomer(customer, targetLaneId, null);
    }
  }

  cardClick = (cardId, metadata, laneId) => {
    let findCustomer = this.state.customers.find(customer => {
      return customer.Id == cardId
    })
    console.dir(findCustomer)
    localStorage.setItem('customer', JSON.stringify(findCustomer))

    // QBOTaskApi.getBoard(1).then(response =>{
    QBOTaskApi.getBoard(findCustomer.Id).then(response => {
      console.dir(response);
      this.setState({
        modalData: response.data.lanes,
        selectCustomer: findCustomer,
        statusChangeId: findCustomer.ParentRef.value,
        employeeChange: this.getEmployeeKey(findCustomer),
      })
    })

    this.modalShow()
  }

  //update lane title
  updateTitle = (laneId, data) => {
    console.dir(laneId)
    console.dir(data.title)
    axios.put(`/Customer/Lane`, { id: laneId, title: data.title })
      .then((response) => {
        console.log(response)
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  modalShow = () => {
    this.setState({
      modalOpen: true
    })
  }

  modalHide = () => {
    this.setState({
      modalOpen: false,
      modalData: [],
      taskID: null,
    })
  }

  importModalShow = () => {
    this.setState({
      importModalOpen: true
    })
  }

  importModalHide = () => {
    this.setState({
      importModalOpen: false,
    })
  }

  openTab = () => {
    QuickbooksApi.getCustomerById(this.state.selectCustomer.Id).then(response => {
      console.dir(response.Result)
      localStorage.setItem('customer', JSON.stringify(response.Result))
      window.open(openNewTab, '_blank')
    })
  }

  setTaskId = (taskId) => {
    console.dir(taskId)
    this.setState({
      taskID: taskId
    })
    // localStorage.setItem('taskId', taskId)
  }

  selectChangeStatusId = (option, index) => {
    console.dir(index)
    console.dir(index.key != '')

    this.setState({
      statusChangeId: index.key
    })
  }

  selectChangeEmployee = (option, index) => {
    console.dir(index)
    console.dir(index.key != '')

    this.setState({
      employeeChange: index.key
    })
  }

  private filterDate(boardObj: { lanes: {}; }, filterList: string[], filterEmployeeList: string[], filterUrgent: string[]) {
    let filterData = JSON.parse(JSON.stringify(boardObj));
    const searchCustomer = this.state.searchCustomer
    console.dir(searchCustomer)
    if (filterList.length != 0 || filterEmployeeList.length != 0 || filterUrgent.length != 0) {
      if (filterList.length != 0) {
        filterData.lanes = filterData.lanes.filter(item => {
          return filterList.indexOf(item.id) >= 0;
        });
      }
      if (filterEmployeeList.length != 0) {
        let tmpLanes: any[] = []
        filterData.lanes.forEach(lane => {
          lane.cards = lane.cards.filter(card => {
            let employee = card.description.split(': ')
            if (employee.length >= 2) {
              employee = employee[1]
              return filterEmployeeList.indexOf(employee) >= 0
            }
          })
          tmpLanes.push(lane)
        });
        filterData.lanes = tmpLanes
      }

      if (filterUrgent.length != 0) {
        let tmpLanes: any[] = []
        filterData.lanes.forEach(lane => {
          lane.cards = lane.cards.filter(card => {
            let findTaskStatus = card.taskStatusList.find(task => {
              return filterUrgent.indexOf(task.board_lane_id.toString()) >= 0
            })
            if (findTaskStatus)
              return true
          })
          tmpLanes.push(lane)
        });
        filterData.lanes = tmpLanes
      }
    }

    if (searchCustomer != null && searchCustomer != '') {
      let tmpLanes: any[] = []
      filterData.lanes.forEach(lane => {
        lane.cards = lane.cards.filter(card => {
          //return card.id === searchCustomer
          return card.title.includes(searchCustomer)
        })
        if (lane.cards.length >= 1) {
          tmpLanes.push(lane)
        }
      });
      filterData.lanes = tmpLanes
    }

    return filterData;
  }

  private updateCustomer(customer: any, targetLaneId: any, changeEmployee: string | null) {

    return customer;
  }

  getEmployeeKey(customer) {
    if (customer.DisplayName) {
      if (customer.DisplayName.split('-').length >= 2 && customer.DisplayName[2] === '-') {
        return customer.DisplayName.split('-')[0]
      } else {
        return ''
      }
    } else {
      return ''
    }
  }

  private getEmployeeOption() {
    const activeEmployees = this.state.employee.filter(employee => {
      return employee.displayNameField.indexOf('*') == -1
    })
    const employeeOption = activeEmployees.map(employee => {
      return {
        key: `${employee.givenNameField[0]}${employee.familyNameField[0]}`,
        text: employee.givenNameField + ' ' + employee.familyNameField,
        email: employee.primaryEmailAddrField ? employee.primaryEmailAddrField.addressField : '',
        display: employee.displayNameField,
      }
    })


    let spUsers = this.state.allSharePointUsers.slice()
    const spUsersOption = spUsers.map(employee => {
      return {
        key: `${employee.givenName[0]}${employee.surname[0]}`,
        text: employee.givenName + ' ' + employee.surname,
        email: employee.mail ? employee.mail : '',
        display: employee.givenName + ' ' + employee.surname
      }
    })
    //console.log(employeeOption)
    //console.log(spUsers)

    //let idSet = new Set(spUsers.map(o => o.mail));
    //let employees_ = employeeOption.map(o => ({ ...o, email: idSet.has(o.email) ? o.email : '' }));
    let employees__ = spUsersOption.filter(emp => emp.email && emp.email != 'admin@witterassociates.com' && emp.email != 'seany_ascydesign.onmicrosoft.com#ext#@witterassociates.com')
    employees__.unshift({ key: '', text: 'Not Assigned', email: '', display: '', })

    return employees__
  }
  private filterEmployeeOption() {
    let result = this.getEmployeeOption()
    result.shift()
    return result
  }

  TimerModalClose() {
    this.setState({ isTimerModalOpen: false })
  }

  refreshToken() {
    this.setState({ isTimerModalOpen: false, })
    localStorage.setItem('TokenTimer', moment().add(Setting.tokenExpired, 'm').format('LLLL'))

    QuickbooksApi.getToken().then(response => {
      let access_token = (response as unknown as RefreshToken).quickBooksInfo.access_token
      this.setState({
        accessToken: access_token
      })
      localStorage.setItem('token', access_token)
    })
  }

  tokenTimer() {
    let expiredTime = moment(localStorage.getItem('TokenTimer'))
    let remind = expiredTime.add(-Setting.tokenRemindExpired, 'm')
    if (moment().isAfter(remind)) {
      if (!this.state.isTimerModalOpen) {
        this.setState({ isTimerModalOpen: true })
      }
    } else {
      if (this.state.isTimerModalOpen) {
        this.setState({ isTimerModalOpen: false })
      }
    }
  }

  saveCard = async (card: { id?: any; lane_id: any; position: any; customer_qb_id?: null; status_qb_id?: any; reminder_id?: any; description?: any; due_date?: any; priority?: any; title?: any; active?: any; }) => {
    await axios.put('/Customer/Card', card)
  }
}

function getLanes(customers, taskStatus, employeeOption, customerBoardLane) {
  let boardObj = {
    lanes: {}
  }

  customerBoardLane.lanes = customerBoardLane.lanes.map(lane => {
    console.dir(lane)
    lane.cards = lane.cards.map(card => {
      console.dir(card)

      let findCustomer = customers.find(customer => {
        return customer.Id == card.customer_qb_id
      })

      console.dir(findCustomer)

      return {
        description: '',
        id: findCustomer.Id,
        taskStatusList: [],
        title: findCustomer.DisplayName,
        laneId: lane.id,
        cardId: card.id,
        detail: findCustomer,
      }
    })
    return lane
  })


  boardObj.lanes = customerBoardLane.lanes

  // console.dir(customerBoardLane.lanes)
  // console.dir(boardObj)

  return boardObj
}

const ScrollableLane = styled.div`
  flex: 1;
  overflow-y: auto;
  min-width: 100%;
  overflow-x: hidden;
  align-self: center;
  max-height: 90vh;
  margin-top: 10px;
  flex-direction: column;
  justify-content: space-between;
`

function getOption(data): [any] {
  const result = data.map(item => {
    return {
      key: item.id,
      text: item.title,
    }
  })

  // result.unshift({ key: '', text: '' })

  return result
}

function customersSort(customers: any): any {
  customers.sort(function (a, b) {
    if (a.Level == '1') {
      return 0
    } else {
      let aDisplayName = a.DisplayName.indexOf('-') == 2 ? a.DisplayName.substr(3) : a.DisplayName
      let bDisplayName = b.DisplayName.indexOf('-') == 2 ? b.DisplayName.substr(3) : b.DisplayName

      if (aDisplayName > bDisplayName) {
        return 1;
      } else {
        return -1
      }
    }
  });
  return customers
}
