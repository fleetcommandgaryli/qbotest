import * as React from 'react';
import styles from '../../styles/QboConnect.module.scss';
import { IQboConnectProps, IQboConnectStates } from '../../interfaces/IQboConnectProps';
import { QuickbooksApi } from '../../api/QuickbooksApi';
import { Setting } from '../../utils/Setting';
import Layout from '../../components/Layout';
import Image from 'next/image'
import defaultPic from '../../utils/images/C2QB_green_btn_lg_default.png'
import hoverPic from '../../utils/images/C2QB_green_btn_lg_default.png'
import { NotifyMessage } from '../../utils/notify/notifyMessage';
import { mergeStyles, AnimationStyles } from '@fluentui/react';
import { Fragment } from 'react';
import moment from 'moment';

export default class QboConnect extends React.Component<IQboConnectProps, IQboConnectStates> {
  constructor(props: IQboConnectProps) {
    super(props);
    this.state = {
      notify: '',
    }
  }

  public render(): React.ReactElement<IQboConnectProps> {
    return (
      <Fragment>
        <div>
          <div className={fadeIn} onClick={this.connect}>
            <Image src={defaultPic}
              onMouseOver={e => e.currentTarget.src = `${defaultPic}`}
              onMouseOut={e => e.currentTarget.src = `${hoverPic}`} />
          </div>
          <NotifyMessage str={this.state.notify.str} class={this.state.notify.class}></NotifyMessage>
        </div>
      </Fragment>


    );
  }

  componentDidMount() {
    QuickbooksApi.afterLoginGetToken().then(async response => {
      let { result, realmId } = response as any
      result = await result

      console.dir(result)
      console.dir(realmId)

      if (result != 'no code') {
        let thisUrl = window.location.protocol + "//" + window.location.host + window.location.pathname
        window.history.replaceState({ path: thisUrl }, '', thisUrl);

        let { tokenResponse, tokenViewModal, userInfoResponse } = result as any;

        if (tokenViewModal.IsError == true) {
          console.dir('Link Failed')
          this.errorMessage('Link Failed')
        }
        else {
          console.dir('Linked to Quickbooks Online Success')
          this.successMessage('Linked to Quickbooks Online Success')
          //window.location.href = Setting.clientPage

          console.dir(result)
          const response = result as any
          localStorage.setItem('company_code', realmId)
          localStorage.setItem('token', tokenViewModal.AccessToken)
          localStorage.setItem('TokenTimer', moment().add(Setting.tokenExpired, 'm').format('LLLL'))

          // const userInfoData = await fetch('api/GetUserInfo', {
          //   method: 'GET',
          //   headers: new Headers({
          //     'Content-Type': 'application/json',
          //     'Authorization': `Bearer ${result.tokenResponse.AccessToken}`
          //   })
          // })
          // let userInfo = await userInfoData.json()
          // console.dir(userInfo.Json)
          // localStorage.setItem('userInfo', JSON.stringify(userInfo.Json))

          console.dir(userInfoResponse.Json)
          localStorage.setItem('userInfo', JSON.stringify(userInfoResponse.Json))


        }
      }
    })
  }

  connect() {
    QuickbooksApi.connect()
  }

  private errorMessage(str: string) {
    this.setState({
      notify: { str: str, class: 'error' }
    });

    setTimeout(() => {
      this.setState({
        notify: { str: '', class: 'error' }
      });
    }, 5000);
  }

  private successMessage(str: string) {
    this.setState({
      notify: { str: str, class: 'success' }
    });

    setTimeout(() => {
      this.setState({
        notify: { str: '', class: 'success' }
      });
    }, 5000);
  }
}

const fadeIn = mergeStyles(AnimationStyles.fadeIn400);
