import { PrimaryButton } from '@fluentui/react'
import Link from 'next/link'
import React, { Fragment, useEffect, useState } from 'react'
import Layout from '../components/Layout'
import { QuickbooksApi } from '../api/QuickbooksApi';
import GetUserInfo from '../pages/api/GetUserInfo';

const AboutPage = () => {

  const [userInfo, setUserInfo] = useState('')

  useEffect(() => {
    // QuickbooksApi.getUserInfo().then(response => {
    //   console.dir(response)

    // })

    fetchData()

  }, [])

  const fetchData = async () => {
    const data = await fetch('api/GetUserInfo')
    let dd = await data.json()
    console.dir(dd)
    setUserInfo(dd.Json)
  }

  return (
    <Fragment>
      <h1>About</h1>
      <p>This is the about page</p>
      <p>
        <Link href="/">
          <div>
            <a>Go home</a>
            <PrimaryButton>Button</PrimaryButton>
          </div>
        </Link>
      </p>
    </Fragment>
  )
}

export default AboutPage
