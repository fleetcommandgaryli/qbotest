import type { NextPage } from 'next';
import { QuickbooksApi } from '../api/QuickbooksApi';
import React, { Fragment, useEffect, useState } from 'react';
import Image from 'next/image';
import defaultPic from '../utils/images/C2QB_green_btn_lg_default.png';
import hoverPic from '../utils/images/C2QB_green_btn_lg_default.png';
import moment from 'moment';
import { Setting } from '../utils/Setting';
import { NotifyMessage } from '../utils/notify/notifyMessage';
import { initializeIcons } from '@fluentui/react/lib-commonjs/Icons'


const Home: NextPage = () => {
  const [notify, setNotify] = useState({ str: '', class: 'success' })

  useEffect(() => {
    initializeIcons()

    QuickbooksApi.afterLoginGetToken().then(async response => {
      let { result, realmId } = response as any
      result = await result

      console.dir(result)
      console.dir(realmId)

      if (result != 'no code') {
        let thisUrl = window.location.protocol + "//" + window.location.host + window.location.pathname
        window.history.replaceState({ path: thisUrl }, '', thisUrl);

        let { tokenResponse, tokenViewModal, userInfoResponse } = result as any;

        if (tokenViewModal.IsError == true) {
          console.dir('Link Failed')
          errorMessage('Link Failed')
        }
        else {
          console.dir('Linked to Quickbooks Online Success')
          successMessage('Linked to Quickbooks Online Success')


          console.dir(result)
          const response = result as any
          localStorage.setItem('company_code', realmId)
          localStorage.setItem('token', tokenViewModal.AccessToken)
          localStorage.setItem('TokenTimer', moment().add(Setting.tokenExpired, 'm').format('LLLL'))

          console.dir(userInfoResponse.Json)
          localStorage.setItem('userInfo', JSON.stringify(userInfoResponse.Json))

          window.location.href = '/qboKanban'

        }
      }
    })
  }, [])

  const errorMessage = (str: string) => {
    setNotify({ str: str, class: 'error' })

    setTimeout(() => {
      setNotify({ str: '', class: 'error' })
    }, 5000);
  }

  const successMessage = (str: string) => {
    setNotify({ str: str, class: 'success' })

    setTimeout(() => {
      setNotify({ str: '', class: 'success' })
    }, 5000);
  }



  return (
    <Fragment>
      <div onClick={connect} style={{ cursor: 'pointer', display: 'flex', justifyContent: 'center' }}>
        <Image src={defaultPic}
          onMouseOver={e => e.currentTarget.src = `${defaultPic}`}
          onMouseOut={e => e.currentTarget.src = `${hoverPic}`} />
      </div>
      <NotifyMessage str={notify.str} class={notify.class}></NotifyMessage>
    </Fragment>
  )
}

const connect = () => {
  QuickbooksApi.connect()
}

export default Home
