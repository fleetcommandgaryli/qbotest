import moment from 'moment';
import { Setting } from '../../../utils/Setting';

type RefreshToken = {
	quickBooksInfo: QuickBooksInfo
}

type QuickBooksInfo = {
	access_token: string;
	client_id: string;
}

export class QuickbooksApi {
	public static apiUrl = Setting.apiUrl

	public static connect(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?login=true`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json'
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				console.dir(response)

				window.location.href = response
			})
		})
	}

	public static getToken(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?refreshToken=true`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json'
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				// console.dir(response);
				if (response.quickBooksInfo.access_token_expires_at == '0') {
					window.location.href = Setting.qboConnectUrl
				}
				resolve(response)
				// this.setState({
				// 	accessToken: (response as unknown as RefreshToken).quickBooksInfo.access_token
				// })
			})


		})
	}

	public static updateUserAccessToken(user: any, token: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint`, {
				method: 'PATCH',
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
				body: JSON.stringify({ user: user, token: token })
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
			})
		})
	}

	public static getAllCustomer(): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?account_id=0`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static getCustomerById(id: any, token: never): Promise<any | void> {
		console.log('API------------------------')
		console.log(token)

		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?account_id=${id}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				// this.autoUpdateToken()
			})
		})
	}

	public static getAllCustomerByPage(page: any, includeInactive: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?account_id=-1&page=${page}&includeInactive=${includeInactive}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static getAllCustomerQuery(query: any, includeInactive: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}GetCustomerQuery?query=${query}&includeInactive=${includeInactive}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static searchCustomer(value: any, includeInactive: any): Promise<any | void> {
		//0910
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SearchCustomer`, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
				body: JSON.stringify({searchValue:value, includeInactive: includeInactive})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static getAllCustomerCount(includeInactive: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}GetCustomerCount?includeInactive=${includeInactive}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static updateCustomer(customer: { idField: any; }): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?customer_id=${customer.idField}`, {
				method: 'PUT',
				body: JSON.stringify(customer),
				headers: new Headers({
					'Content-Type': 'application/json'
				})
			}).then(res => {
				console.dir(res)
				if (res.status == 200)
					return res.json();
				else
					reject(res.json())

			})
				.catch(error => {
					console.dir(error)
					reject(error)
				})
				.then(response => {
					console.dir(response)
					resolve(response)
					this.autoUpdateToken()
				})
		})
	}

	public static kanBanUpdateCustomer(customer: { Id: any; }): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}KanBanUpdateCustomer?customer_id=${customer.Id}`, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
				body: JSON.stringify(customer)
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static getEntity(entities: string, accessToken: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?entities=${entities}&token=${accessToken}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static getTimeActivity(timeActivity_cusotmer_id: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?timeActivity_cusotmer_id=${timeActivity_cusotmer_id}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static createOrUpdateTimeActivity(timeActivity: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint`, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
				body: JSON.stringify(timeActivity)
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static deleteTimeActivity(timeActivity: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?operation=delete`, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
				body: JSON.stringify(timeActivity)
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static afterLoginGetToken() {
		return new Promise((resolve, reject) => {
			let code = this.getParameterByName('code', null)
			console.dir(code)

			if (code == null)
				resolve('no code')
			else {
				let result = this.getTokenByCode(code)
				resolve(result)
			}
		})
	}

	public static getTokenByCode(code: string) {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?code=${code}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
			})
		})
	}

	public static getQuickBooksInfo() {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
			})
		})
	}

	public static getParameterByName(name: string, url: string | null) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	public static getTimeActivityByDate(startDate: string, endDate: string | null = null): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?startDate=${startDate}&endDate=${endDate}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(response)
				this.autoUpdateToken()
			})
		})
	}

	public static getProfitAndLoss(companyCode: string, accessToken: string, dateMacro: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?companyCode=${companyCode}&accessToken=${accessToken}&dateMacro=${dateMacro}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(JSON.parse(response.Content))
				this.autoUpdateToken()
			})
		})
	}

	public static getProfitAndLossByDate(companyCode: string, accessToken: string, startDate: string, endDate: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?companyCode=${companyCode}&accessToken=${accessToken}&startDate=${startDate}&endDate=${endDate}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(JSON.parse(response.Content))
				this.autoUpdateToken()
			})
		})
	}

	public static getClassSales(companyCode: string, accessToken: string, dateMacro: string, summarizeBy: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?companyCode=${companyCode}&accessToken=${accessToken}&dateMacro=${dateMacro}&summarizeBy=${summarizeBy}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(JSON.parse(response.Content))
				this.autoUpdateToken()
			})
		})
	}

	public static getClassSalesByDate(companyCode: string, accessToken: string, startDate: string, endDate: string, summarizeBy: string): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?companyCode=${companyCode}&accessToken=${accessToken}&startDate=${startDate}&endDate=${endDate}&summarizeBy=${summarizeBy}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(JSON.parse(response.Content))
				this.autoUpdateToken()
			})
		})
	}

	public static getInvoiceByDate(startDate: string, endDate: string | null = null): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?getInvoice=true&startDate=${startDate}&endDate=${endDate}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(JSON.parse(response.Content))
				this.autoUpdateToken()
			})
		})
	}

	public static getInvoiceByCustomerId(customerId: any): Promise<any | void> {
		return new Promise((resolve, reject) => {
			fetch(`${this.apiUrl}SharePoint?customerId=${customerId}`, {
				method: 'GET',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}).then(res => {
				return res.json();
			}).catch(error => {
				console.dir('Error:', error)
				reject(error)
			}).then(response => {
				resolve(JSON.parse(response.Content))
				this.autoUpdateToken()
			})
		})
	}

	public static getDateRange(dateMacro: string) {
		let result = { startDate: '', endDate: '' };

		if (dateMacro == 'Last 30 days') {
			result.startDate = moment().add(-29, 'days').format('YYYY-MM-DD')
			result.endDate = moment().format('YYYY-MM-DD')
		} else if (dateMacro == 'Last 7 days') {
			result.startDate = moment().add(-6, 'days').format('YYYY-MM-DD')
			result.endDate = moment().format('YYYY-MM-DD')
		} else if (dateMacro == 'Last 14 days') {
			result.startDate = moment().add(-13, 'days').format('YYYY-MM-DD')
			result.endDate = moment().format('YYYY-MM-DD')
		} else if (dateMacro == 'This Month') {
			result.startDate = `${moment().format('YYYY-MM-')}01`
			result.endDate = moment(`${moment().add(1, 'months').format('YYYY-MM-')}01`).add(-1, 'day').format('YYYY-MM-DD')
		} else if (dateMacro == 'This Fiscal Quarter') {
			let thisYear = moment().format('YYYY')
			let thisMonth = parseInt(moment().format('MM'))

			result.startDate = moment(`${thisYear}-${(Math.ceil(thisMonth / 3) - 1) * 3 + 1}-01`).format('YYYY-MM-DD')
			result.endDate = moment(`${thisYear}-${(Math.ceil(thisMonth / 3)) * 3}-01`).add(1, 'months').add(-1, 'days').format('YYYY-MM-DD')
		}
		else if (dateMacro == 'This Fiscal Year') {
			let thisYear = moment().format('YYYY')

			result.startDate = `${thisYear}-01-01`
			result.endDate = `${thisYear}-12-31`
		}
		else if (dateMacro == 'Last Month') {
			let thisYear = moment().format('YYYY')
			let thisMonth = parseInt(moment().format('MM'))

			result.startDate = moment(`${thisYear}-${thisMonth}-01`).add(-1, 'months').format('YYYY-MM-DD')
			result.endDate = moment(`${thisYear}-${thisMonth}-01`).add(-1, 'days').format('YYYY-MM-DD')
		} else if (dateMacro == 'Last Fiscal Quarter') {
			let thisYear = moment().format('YYYY')
			let thisMonth = parseInt(moment().format('MM'))

			result.startDate = moment(`${thisYear}-${(Math.ceil(thisMonth / 3) - 1) * 3 + 1}-01`).add(-3, 'months').format('YYYY-MM-DD')
			result.endDate = moment(`${thisYear}-${(Math.ceil(thisMonth / 3)) * 3}-01`).add(-2, 'months').add(-1, 'days').format('YYYY-MM-DD')
		} else if (dateMacro == 'Last Fiscal Year') {
			let lastYear = parseInt(moment().format('YYYY')) - 1

			result.startDate = `${lastYear}-01-01`
			result.endDate = `${lastYear}-12-31`
		} else if (dateMacro == 'current pay period') {
			let periodStart = Setting.periodStart
			let periodNumber = Setting.periodNumber
			let thisPeriodStart = moment().add(-(moment().diff(moment(periodStart), 'days') % periodNumber), 'days').format('YYYY-MM-DD')
			let thisPeriodEnd = moment(thisPeriodStart).add(periodNumber - 1, 'days').format('YYYY-MM-DD')

			result.startDate = moment(thisPeriodStart).format('YYYY-MM-DD')
			result.endDate = moment(thisPeriodEnd).format('YYYY-MM-DD')
		} else if (dateMacro == 'previous period') {
			let periodStart = Setting.periodStart
			let periodNumber = Setting.periodNumber
			let thisPeriodStart = moment().add(-(moment().diff(moment(periodStart), 'days') % periodNumber), 'days').format('YYYY-MM-DD')
			let thisPeriodEnd = moment(thisPeriodStart).add(periodNumber - 1, 'days').format('YYYY-MM-DD')

			result.startDate = moment(thisPeriodStart).add(-periodNumber, 'days').format('YYYY-MM-DD')
			result.endDate = moment(thisPeriodEnd).add(-periodNumber, 'days').format('YYYY-MM-DD')
		}

		return result
	}

	private static autoUpdateToken() {
		let expiredTime = moment(localStorage.getItem('TokenTimer'))
		let autoUpdateTime = expiredTime.add(-Setting.tokenAutouUpdate, 'm')
		if (moment().isAfter(autoUpdateTime)) {
			// localStorage.setItem('TokenTimer', moment().add(Setting.tokenExpired, 'm').format('LLLL'))
			QuickbooksApi.getToken()
		}
	}
}