// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { Setting } from '../../utils/Setting'

// type Data = {
//   name: string
// }

// export default function handler(
//   req: NextApiRequest,
//   res: NextApiResponse<Data>
// ) {
//   res.status(200).json({ name: 'John Doe' })
// }

const apiUrl = Setting.apiUrl

async function GetUserInfo() {
    return fetch(`${apiUrl}GetUserInfo`, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json',
            // 'Authorization': `Bearer ${localStorage.getItem('token')}`
        })
    }).then(res => {
        // console.log(res)
        return res.json();
    }).catch(error => {
        console.dir('Error:', error)
        return (error)
    }).then(response => {
        // console.log(response)
        return (response)
        // this.autoUpdateToken()
    })
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    let result = await GetUserInfo()
    res.status(200).json(result)
}