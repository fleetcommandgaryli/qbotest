import React, { Fragment } from 'react';
import Board from 'react-trello';
import { QuickbooksApi } from '../../api/QuickbooksApi';
import { IQboTasksProps } from '../../interfaces/IQboTasksProps';
import { CardModal } from '../../components/TasksComponents/Modal/Modal';
import NewCardForm from '../../components/TasksComponents/NewCardForm/NewCardForm';
import CustomCard from '../../components/TasksComponents/CustomCard/CustomCard';
import { customer } from '../../interfaces/IQboCustomerProps';
import axios from 'axios';
import { sessionStorageListener } from '../../utils/SessionStorageListener';
import { CommandBarButton, IContextualMenuProps, IContextualMenuItem, DirectionalHint, IDropdownStyles, IDropdownOption } from '@fluentui/react';
// import { sp } from "@pnp/sp/presets/all";
import { Loading } from '../../utils/notify/notifyMessage';
import testdata from "../../components/TasksComponents/data.json"
import { GraphApi } from '../../api/GraphApi';
import { Setting } from '../../utils/Setting';
import moment from 'moment';
import { initializeIcons } from '@fluentui/react/lib-commonjs/Icons';
import { LoginCheck, LogOut } from '../../utils/LoginUtil';

type sessionStorageObj = {
  key: string;
  value: string;
}

var spObj = null;
let eventBus: { publish: (arg0: { type: string; laneId: any; cardId?: any; card?: { label: string; laneId: any; comments_count: any; checklist_checked_count: any; checklist_count: any; metadata: any; attachments_count: number; id: any; lane_id: any; title: any; status_qb_id: any; due_date: any; priority: any; description: any; is_template: any; reminder_id: any; }; }) => void; } | undefined = undefined


export default class QboTasks extends React.Component<IQboTasksProps, {}> {
  constructor(props: any) {
    super(props)
    axios.defaults.baseURL = Setting.apiUrl;
    initializeIcons();

    // sp.setup({
    //   spfxContext: this.props.spcontext
    // });
    // spObj = sp;
  }

  state = {
    data: { lanes: [{ id: 'loading', title: 'Loading...', style: { height: 80 }, cards: [] }] },
    displayData: { lanes: [{ id: 'loading', title: 'Loading...', style: { height: 80 }, cards: [] }] },
    testdata: testdata,
    editable: false,
    currentUser: {
      name: "User User",
      initials: "UU"
    },
    opened: false,
    task_loading: false,
    access_token: null,
    Users: [],
    unassignedUsers: [],
    attachments: [],
    Card: {
      id: null,
      lane_id: null,
      title: null,
      label: null,
      status_qb_id: null,
      due_date: null,
      priority: null,
      metadata: null,
      description: null,
      is_template: null,
      reminder_id: null,
      comments_count: null,
      checklist_checked_count: null,
      checklist_count: null,
      customer_qb_id: null
    },
    Customer: {
      qb_id: null,
      status_qb_id: null,
      name: null,
      status: ''
    },
    comments: [] as Array<any>,
    checklist: [] as Array<any>,
    addCommentBtnDisabled: true,
    typingComment: "",
    typingChecklist: "",
    hidePersona: false,
    filterMenu0: [] as Array<any>,
    filterMenu0Value: [] as Array<any>,
    filterMenu1: [] as Array<any>,
    filterMenu1Value: [] as Array<any>,
    filterMenuNumber: [0, 0],
    filterNumber: 0,
    usersItem: [] as Array<any>,
    status_filter: '',
    assignment_filter: '',
    phases_level1: [] as Array<any>,
    phases_level2: [] as Array<any>,
    attachmentLoading: false,
    alert_recipients: [] as Array<any>,
    filterDay: '30',
    boardLane: [],
  }

  setEventBus = (handle: any) => {
    eventBus = handle
  }

  onToggleSelect = (ev?: React.MouseEvent<HTMLButtonElement>, item?: IContextualMenuItem) => {
    ev && ev.preventDefault();

    let menu_number = this.state.filterMenuNumber.slice()
    let filterNumber_ = this.state.filterNumber
    if (item) {
      let status = item.key.substring(0, 6)
      let category = 0
      if (status == 'assign')
        category = 1
      let num = category == 0 ? item.key.substring(6, item.key.indexOf('|')) : item.key.substring(6)

      let menu = category == 1 ? this.state.filterMenu1.slice() : this.state.filterMenu0.slice()
      let menu_value = category == 1 ? this.state.filterMenu1Value.slice() : this.state.filterMenu0Value.slice()

      menu[num] = !menu[num]
      if (menu[num] == true) {
        filterNumber_++
        menu_number[category]++
        if (category == 1)
          menu_value.push(item.text)
        else
          menu_value.push(item.key.substring(item.key.indexOf('|') + 1, item.key.length))
      }
      else {
        filterNumber_--
        menu_number[category]--

        let index = menu_value.indexOf(item.text);

        if (category != 1) {
          let key = this.state.phases_level2.find(statusItem => {
            return statusItem.text == item.text
          }).key

          index = menu_value.indexOf(key);
        }

        if (index > -1) {
          menu_value.splice(index, 1);
        }
      }
      if (category == 1)
        this.setState({ filterMenu1: menu, filterMenu1Value: menu_value, filterMenuNumber: menu_number, filterNumber: filterNumber_ })
      else
        this.setState({ filterMenu0: menu, filterMenu0Value: menu_value, filterMenuNumber: menu_number, filterNumber: filterNumber_ })
      //returns an array of selected users
      let i = 0
      let str = ""
      while (i < menu_value.length) {
        str += "'" + menu_value[i] + "',"
        i++
      }
      if (str)
        str = str.slice(0, -1)
      if (category == 1)
        this.setState({ assignment_filter: str })
      else if (category == 0) {
        console.dir(str)
        this.setState({ status_filter: str })
      }

      // setTimeout(this.getBoard.bind(this), 1000)
    }
  }

  // method to get current user  
  // private async getCurrentUser() {
  //   if (Setting.apiUrl == `http://localhost/SharePoint/api/` || Setting.apiUrl == `https://localhost:44398/api/`) {
  //     GraphApi.getCurrentUser().then(user => {
  //       let name = (user as any).Title
  //       var names = name.split(' '),
  //         initials = names[0].substring(0, 1).toUpperCase();
  //       if (names.length > 1) {
  //         initials += names[names.length - 1].substring(0, 1).toUpperCase();
  //       }

  //       this.setState({ currentUser: { name: (user as any).Title, initials: initials } });
  //     })

  //   } else {
  //     let user = await sp.web.currentUser.get();
  //     let name = user.Title

  //     var names = name.split(' '),
  //       initials = names[0].substring(0, 1).toUpperCase();

  //     if (names.length > 1) {
  //       initials += names[names.length - 1].substring(0, 1).toUpperCase();
  //     }
  //     //console.log('name is: ' +name)
  //     this.setState({ currentUser: { name: user.Title, initials: initials } });
  //   }
  // }

  addCard = (card: any, laneId: any) => {
    let newCard = {
      temp_id: card.id,
      lane_id: laneId,
      title: card.title,
      template: card.template,
      customer_qb_id: card.customer,
      status_qb_id: this.state.Customer.status_qb_id,
      created_by: this.state.currentUser.name
    }

    console.dir(card)
    console.dir(newCard)

    axios.post('/Task/Card', newCard)
      .then((response) => {
        setTimeout(this.getBoard.bind(this), 1)
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  clickCard = (cardId: any, metadata: any, laneId: any) => {
    this.setState({ opened: true })
    //Get card info
    let card = null as any;
    const cardPromise = new Promise<void>((resolve, reject) => {
      axios.get('/Task/Card?card_id=' + cardId)
        .then((response) => {
          card = response.data
          resolve()
        })
        .catch(function (error) {
          console.log(error);
          reject()
        })
        .then(() => {
        })
    });

    //Get comments
    const commentPromise = new Promise<void>((resolve, reject) => {
      axios.get('/Task/Comment?card_id=' + cardId)
        .then((response) => {
          this.setState({ comments: response.data })
          resolve()
        })
        .catch(function (error) {
          console.log(error);
          reject()
        })
        .then(() => {
        })
    });

    //Get attachments
    this.getAttachments(cardId, true)

    //Get assigned and unassigned
    let hidePersona = false;
    Promise.all([cardPromise, commentPromise]).then((values) => {
      let a = card.metadata
      let b = this.state.Users.slice()
      if (a.length > 3)
        hidePersona = true;
      for (var i = 0, len = a.length; i < len; i++) {
        for (var j = 0, len2 = b.length; j < len2; j++) {
          if (a[i].text === b[j].text) {
            b.splice(j, 1);
            len2 = b.length;
          }
        }
      }

      this.setState({
        opened: true,
        unassignedUsers: b,
        hidePersona: hidePersona,
        checklist: card.checklist,
        alert_recipients: card.alert_recipients,
        Card: {
          id: card.id,
          lane_id: card.lane_id,
          status_qb_id: card.status_qb_id,
          title: card.title,
          label: card.label,
          due_date: card.due_date,
          priority: card.priority,
          metadata: JSON.stringify(card.metadata),
          description: card.description,
          is_template: card.is_template,
          reminder_id: card.reminder_id,
          comments_count: card.comments_count,
          checklist_checked_count: card.checklist_checked_count,
          checklist_count: card.checklist_count,
          customer_qb_id: card.customer_qb_id
        }
      })
    });

  }

  dragCardEnd = (cardId: any, sourceLaneId: any, targetLaneId: any, position: any, cardDetails: any) => {
    //fire api and save the new card position
    let card = { id: cardId, lane_id: targetLaneId, position: position, customer_qb_id: cardDetails.customer_qb_id }
    this.saveCard(card)
  }

  onPopupClosed = (event: any) => {
    this.setState({
      opened: false,
      attachments: [],
      checklist: [],
      comments: [],
      Card: {
        id: null,
        lane_id: null,
        title: null,
        label: null,
        status_qb_id: null,
        due_date: null,
        priority: null,
        metadata: null,
        description: null,
        is_template: null,
        reminder_id: null,
      }
    })
  }

  getQuickbooksCustomerName = (customer_qb_id: string | null) => {
    if (customer_qb_id) {
      console.dir(customer_qb_id)
      console.dir(this.state.phases_level2)
      let findQuickbooksCustomer = this.state.phases_level2.find(customer => {
        return customer.key === customer_qb_id
      })

      return findQuickbooksCustomer ? findQuickbooksCustomer.text : ''
    } else
      return ''
  }

  addMember = (name: any, initials: any) => {
    let b = this.state.unassignedUsers.slice()
    let a = JSON.parse(this.state.Card.metadata as any)
    let hidePersona = this.state.hidePersona

    //call API
    axios.post('/Task/User', { name: name, card_id: this.state.Card.id })
      .then((response) => {

      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        setTimeout(this.getBoard.bind(this), 10)
      });

    for (var j = 0, len2 = b.length; j < len2; j++) {
      if (name === b[j].text) {
        b.splice(j, 1);
        len2 = b.length;
        a.push({ "key": name, "text": name, "imageInitials": initials })
        if (a.length > 3)
          hidePersona = true
      }
    }

    var newCard = { ...this.state.Card }
    newCard.metadata = JSON.stringify(a) as any
    this.setState({ Card: newCard, unassignedUsers: b, hidePersona: hidePersona })
  }

  deleteMember = (name: any, initials: any) => {
    let a = this.state.unassignedUsers.slice()
    let b = JSON.parse(this.state.Card.metadata as any)
    let hidePersona = this.state.hidePersona
    //call API
    axios.put('/Task/User', { name: name, card_id: this.state.Card.id })
      .then((response) => {
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        setTimeout(this.getBoard.bind(this), 10)
      });

    for (var j = 0, len2 = b.length; j < len2; j++) {
      if (name === b[j].text) {
        b.splice(j, 1);
        len2 = b.length;
        a.push({ "text": name, "imageInitials": initials })
        if (b.length <= 3)
          hidePersona = false
      }
    }

    var newCard = { ...this.state.Card }
    newCard.metadata = JSON.stringify(b)
    this.setState({ Card: newCard, unassignedUsers: a, hidePersona: hidePersona })
  }

  saveRecipients = (alert_recipients: any) => {
    axios.post('/Task/Alert', alert_recipients)
      .then((response) => {
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  addMention = (card_id: string, checklist_id: any, text: any) => {
    // make a copy of the checklist from state
    let list = this.state.checklist.slice()
    // Call api to add Mention
    axios.post('/Task/Mention', { checklist_id: checklist_id, text: text })
      .then((response) => {
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        // update checklist to state
        axios.get('/Task/Card?card_id=' + card_id)
          .then((response) => {
            this.setState({ checklist: (response.data as any).checklist })
          })
          .catch(function (error) {
            console.log(error);
          })
      });
  }

  deleteMention = (card_id: string, mention_id: any) => {
    axios.put('/Task/Mention', { id: mention_id })
      .then((response) => {
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        // update checklist to state
        axios.get('/Task/Card?card_id=' + card_id)
          .then((response) => {
            this.setState({ checklist: (response.data as any).checklist })
          })
          .catch(function (error) {
            console.log(error);
          })
      });
  }

  getAttachments = (card_id: null, fromClickCard: boolean) => {
    //Get attachments
    let folder_path = GraphApi.itemUrl.replace('items', `root:/Task Attachments/${card_id}`)
    let attachments = []

    GraphApi.getToken()
      .then(accessToken => {
        GraphApi.getFile(accessToken, folder_path)
          .then(result => {
            let item_id = result.id
            GraphApi.getFile(accessToken, `${GraphApi.itemUrl}/${item_id}/children`)
              .then(result => {
                if (!result.error) {
                  attachments = result.value
                  this.setState({
                    attachments: attachments,
                    attachmentLoading: false
                  })
                  if (!fromClickCard) {
                    let newCard = {
                      ...this.state.Card,
                      laneId: this.state.Card.lane_id,
                      comments_count: this.state.Card.comments_count,
                      checklist_checked_count: this.state.Card.checklist_checked_count,
                      checklist_count: this.state.Card.checklist_count,
                      metadata: JSON.parse(this.state.Card.metadata),
                      attachments_count: attachments.length
                    }
                    this.cardUpdate(newCard)
                  }
                }
              })
          })
          .catch(function (error) {
            console.log(error);
          })
      })
  }

  attachmentLoadingBegins = () => {
    this.setState({ attachmentLoading: true })
  }

  addAttachment = () => {
    axios.post('/Task/Attachment', { id: this.state.Card.id })
      .then((response) => {
        this.getAttachments(this.state.Card.id, false)
      })
  }

  deleteAttachment = (item_id: any) => {
    GraphApi.getToken()
      .then(accessToken => {
        GraphApi.deleteFile(item_id, accessToken)
          .then(() => {
            axios.put('/Task/Attachment', { id: this.state.Card.id })
              .then((response) => {
                this.getAttachments(this.state.Card.id, false)
              })
          })
      })
  }

  addComment = (content: any) => {
    let a = this.state.comments.slice();
    let count = this.state.Card.comments_count;
    let cardCopy = this.state.Card;
    cardCopy.comments_count = (parseInt(count) + 1).toString();
    let today = new Date();
    let dd = String(today.getDate());
    let mm = String(today.getMonth() + 1);
    let yyyy = today.getFullYear();
    let hours = today.getHours();
    let minutes = String(today.getMinutes());
    let ampm = hours >= 12 ? 'PM' : 'AM';

    let date = `${mm.length == 2 ? mm : 0 + mm}/${dd}/${yyyy} ${hours}:${minutes.length == 2 ? minutes : 0 + minutes} ${ampm}`;

    let comment = {
      card_id: this.state.Card.id,
      name: this.state.currentUser.name,
      content: content
    }

    axios.post('/Task/Comment', comment)
      .then((response) => {
        console.log(response.data)
        a.unshift({
          "id": response.data,
          "name": this.state.currentUser.name,
          "initials": this.state.currentUser.initials,
          "date": date,
          "content": content
        })
        this.setState({ comments: a, typingComment: "", Card: cardCopy })
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        let newCard = {
          ...this.state.Card,
          laneId: this.state.Card.lane_id,
          comments_count: parseInt(this.state.Card.comments_count),
          checklist_checked_count: this.state.Card.checklist_checked_count,
          checklist_count: this.state.Card.checklist_count,
          metadata: JSON.parse(this.state.Card.metadata),
          attachments_count: this.state.attachments.length
        }
        this.cardUpdate(newCard)
      });
  }

  deleteComment = (id: any) => {
    let comments = this.state.comments.slice()
    let count = this.state.Card.comments_count;
    let cardCopy = this.state.Card;
    cardCopy.comments_count = (parseInt(count) - 1).toString();

    axios.put('/Task/Comment', { id: id })
      .then((response) => {
        for (var j = 0, len = comments.length; j < len; j++) {
          if (id === comments[j].id) {
            comments.splice(j, 1);
            len = comments.length;
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        //setTimeout(this.getBoard.bind(this), 10) 
        this.setState({ comments: comments, Card: cardCopy })
        let newCard = {
          ...this.state.Card,
          laneId: this.state.Card.lane_id,
          comments_count: parseInt(this.state.Card.comments_count),
          checklist_checked_count: this.state.Card.checklist_checked_count,
          checklist_count: this.state.Card.checklist_count,
          metadata: JSON.parse(this.state.Card.metadata),
          attachments_count: this.state.attachments.length
        }
        this.cardUpdate(newCard)
      });
  }

  typeComment = (event: { target: { value: any; }; }) => {
    let btnDisabled = true;
    if (event.target.value)
      btnDisabled = false;
    this.setState({ typingComment: event.target.value, addCommentBtnDisabled: btnDisabled })
  }

  saveCard = (card: { id?: any; lane_id: any; position: any; customer_qb_id?: null; status_qb_id?: any; reminder_id?: any; description?: any; due_date?: any; priority?: any; title?: any; active?: any; }) => {
    let card_ = this.state.Card
    //console.log(card.description + ':desc:' + card_.description)
    //console.log(card.due_date + ':due:' + card_.due_date)    
    const lane_id = this.state.Card.lane_id
    const savePromise = new Promise<void>((resolve, reject) => {
      if (card.status_qb_id)
        card_.status_qb_id = card.status_qb_id
      else if (card.reminder_id)
        card_.reminder_id = card.reminder_id
      else if (card.description != card_.description && (card.description || card.description == ""))
        card_.description = card.description
      else if (card.due_date != card_.due_date && (card.due_date || card.due_date == ""))
        card_.due_date = card.due_date
      else if (card.priority)
        card_.priority = card.priority
      else if (card.lane_id)
        card_.lane_id = card.lane_id
      else if (card.title)
        card_.title = card.title
      resolve()
    });
    Promise.all([savePromise]).then((values) => {
      axios.put('/Task/Card', card)
        .then((response) => {
          if (!card.position && card.position != 0) {
            card_.status_qb_id = card_.status_qb_id == "-1" ? "" : card_.status_qb_id
            this.setState({ Card: card_ })
          }
        })
        .catch(function (error) {
          console.log(error);
        })
        .then(() => {
          console.log({
            type: 'UPDATE_CARD',
            laneId: lane_id,
            card: {
              ...card_,
              laneId: lane_id,
              comments_count: this.state.Card.comments_count,
              checklist_checked_count: this.state.Card.checklist_checked_count,
              checklist_count: this.state.Card.checklist_count,
              metadata: JSON.parse(this.state.Card.metadata),
              attachments_count: this.state.attachments.length
            }
          })
          //deleting a card
          if (card.active == 0) {
            this.cardDelete(lane_id, card);
          }
          //card is dragged, no need to update card
          else if (card.position || card.position == 0 || lane_id != this.state.Card.lane_id)
            setTimeout(this.getBoard.bind(this), 1)
          //updating a card
          else {
            let newCard = {
              ...card_,
              label: card_.due_date != '' ? `${card_.due_date.substring(5, 7)}/${card_.due_date.substring(8, 10)}/${card_.due_date.substring(0, 4)}` : '',
              laneId: lane_id,
              comments_count: this.state.Card.comments_count,
              checklist_checked_count: this.state.Card.checklist_checked_count,
              checklist_count: this.state.Card.checklist_count,
              metadata: JSON.parse(this.state.Card.metadata),
              attachments_count: this.state.attachments.length
            }

            this.cardUpdate(newCard)
          }
        });
    });
  }

  addChecklist = (checklist: { label: any; }) => {
    if (checklist.label) {
      let list = this.state.checklist.slice()
      list.push(checklist)
      axios.post('/Task/Checklist', checklist)
        .then((response) => {
          list[list.length - 1].id = response.data.toString()
          this.setState({ checklist: list, typingChecklist: "" })
        })
        .catch(function (error) {
          console.log(error);
        })
        .then(() => {
          let newCard = {
            ...this.state.Card,
            laneId: this.state.Card.lane_id,
            comments_count: this.state.Card.comments_count,
            checklist_checked_count: this.state.Card.checklist_checked_count,
            checklist_count: parseInt(this.state.Card.checklist_count) + 1,
            metadata: JSON.parse(this.state.Card.metadata),
            attachments_count: this.state.attachments.length
          }

          this.cardUpdate(newCard);
        });
    }
  }

  editChecklist = (label: any, id: any, isChecked: any) => {
    let list = this.state.checklist.slice()

    axios.put('/Task/Checklist', { label: label, id: id, isChecked: isChecked })
      .then((response) => {

      })
      .catch(function (error) {
        console.log(error);
      })

    for (var j = 0, len = list.length; j < len; j++) {
      if (id === list[j].id) {
        if (label)
          list[j].label = label
        if (isChecked) {
          let checked = list[j].isChecked;
          list[j].isChecked = checked === '1' ? '0' : '1';
        }
        len = list.length;
      }
    }
    this.setState({ checklist: list })
    let checked = list.filter(item => item.isChecked == '1').length;
    //update card checklist count
    let newCard = {
      ...this.state.Card,
      laneId: this.state.Card.lane_id,
      comments_count: this.state.Card.comments_count,
      checklist_checked_count: checked,
      checklist_count: this.state.Card.checklist_count,
      metadata: JSON.parse(this.state.Card.metadata),
      attachments_count: this.state.attachments.length
    }
    this.cardUpdate(newCard);
  }

  arrangeChecklist = (checklist: any, card_id: any, source_id: any, destination_id: any) => {
    this.setState({ checklist: checklist })
    axios.put(`/Task/Checklist?card_id=${card_id}&source_id=${source_id}&destination_id=${destination_id}`, {})
      .then((response) => {
        console.log(response)
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  deleteChecklist = (id: any) => {
    let list = this.state.checklist.slice()

    axios.patch('/Task/Checklist', { id: id })
      .then((response) => {
        for (var j = 0, len = list.length; j < len; j++) {
          if (id === list[j].id) {
            list.splice(j, 1);
            len = list.length;
          }
        }
        this.setState({ checklist: list })
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        let newCard = {
          ...this.state.Card,
          laneId: this.state.Card.lane_id,
          comments_count: this.state.Card.comments_count,
          checklist_checked_count: this.state.Card.checklist_checked_count,
          checklist_count: parseInt(this.state.Card.checklist_count) - 1,
          metadata: JSON.parse(this.state.Card.metadata),
          attachments_count: this.state.attachments.length
        }
        this.cardUpdate(newCard);
      });
  }

  makeTemplate = (card_id: any) => {
    let card = this.state.Card
    card.is_template = this.state.Card.is_template == 1 ? 0 : 1

    axios.put('/Task/Template', { id: card_id })
      .then((response) => {
        this.setState({ Card: card })
        setTimeout(this.getBoard.bind(this), 100)
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  getClientPhases = () => {
    // QuickbooksApi.getToken().then(() => {
    const getCustomerType = QuickbooksApi.getEntity('CustomerType', '')
    const getAllCustomer = QuickbooksApi.getAllCustomer()
    let options1: { key: any; text: any; }[] = [];
    let options2: { key: any; text: any; parent_id: any; }[] = [];
    let filterMenu_: boolean[] = [];
    return Promise.all([getCustomerType, getAllCustomer]).then(result => {
      //retrieve the qb_id for the customer type 'Status'
      let customerTypeResult = result[0].Result.filter(function (e: { nameField: string; }) {
        return e.nameField == 'Status'
      })
      let customerType = customerTypeResult[0].idField;
      let customers = result[1].QueryResponse.Customer

      // let phases = customers.filter(function (e: { CustomerTypeRef: { value: any; }; ParentRef: any; }) {
      //   return e.CustomerTypeRef && e.ParentRef && e.CustomerTypeRef.value == customerType
      // });
      let phases = customers;

      let category = customers.filter(function (e: { CustomerTypeRef: { value: any; }; ParentRef: any; }) {
        return e.CustomerTypeRef && !e.ParentRef && e.CustomerTypeRef.value == customerType
      });

      console.dir(phases)
      for (let i = 0; i < phases.length; i++) {
        options2.push({ key: phases[i].Id, text: phases[i].PrintOnCheckName, parent_id: phases[i].ParentRef.value })
        filterMenu_.push(false)
      }
      for (let j = 0; j < category.length; j++) {
        options1.push({ key: category[j].Id, text: category[j].FullyQualifiedName })
      }
      this.setState({ phases_level1: options1, phases_level2: options2, filterMenu0: filterMenu_ })
    })
    // })
  }

  private cardDelete(lane_id: any, card: any) {
    eventBus.publish({ type: 'REMOVE_CARD', laneId: lane_id, cardId: card.id })

    // console.dir('delete card')
    let originData = JSON.parse(JSON.stringify(this.state.data));
    let laneIndex, cardIndex;
    let lane = originData.lanes.find((lane: { id: any; }, index: any) => {
      if (lane.id == lane_id)
        laneIndex = index;
      return lane.id == lane_id;
    });
    let findCard = lane.cards.find((item: { id: any; }, index: any) => {
      if (item.id == card.id)
        cardIndex = index;
      return item.id == card.id;
    });

    originData.lanes[laneIndex].cards.splice(cardIndex, 1);
    this.setState({ data: originData });
  }

  private cardUpdate(newCard: { label: string; laneId: any; comments_count: any; checklist_checked_count: any; checklist_count: any; metadata: any; attachments_count: number; id: any; lane_id: any; title: any; status_qb_id: any; due_date: any; priority: any; description: any; is_template: any; reminder_id: any; }) {
    eventBus.publish({
      type: 'UPDATE_CARD',
      laneId: newCard.laneId,
      card: newCard
    });

    // console.dir('update card');
    let originData = JSON.parse(JSON.stringify(this.state.data));
    let laneIndex, cardIndex;
    let lane = originData.lanes.find((lane: { id: any; }, index: any) => {
      if (lane.id == newCard.lane_id)
        laneIndex = index;
      return lane.id == newCard.lane_id;
    });
    let card = lane.cards.find((item: { id: any; }, index: any) => {
      if (item.id == newCard.id)
        cardIndex = index;
      return item.id == newCard.id;
    });

    originData.lanes[laneIndex].cards[cardIndex] = newCard;
    this.setState({ data: originData });
  }

  getBoard() {
    //redirect from Kanban
    if (localStorage.getItem('taskId'))
      this.clickCard(localStorage.getItem('taskId'), null, null)
    //get the task board data
    // axios.get('/Task/Board?customer_qb_id=' + this.state.Customer.qb_id + '&assignment_filter=' + this.state.assignment_filter + '&status_filter=' + this.state.status_filter)
    axios.defaults.headers.common = { 'Authorization': `Bearer ${localStorage.getItem('token')}}` }
    axios.get('/Task/Board?customer_qb_id=' + this.state.Customer.qb_id + '&assignment_filter=' + '' + '&status_filter=' + '')
      .then((response) => {
        let editable = (response.data as any).lanes[0].id == 'loading' ? false : true;

        //console.dir(response.data)


        this.setState({ data: response.data, editable: editable })
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(() => {
        this.setState({ task_loading: false })
        localStorage.removeItem('taskId')
      });
  }

  filterBoard() {
    let filterData = JSON.parse(JSON.stringify(this.state.data))

    if (filterData.lanes[0].id == 'loading') {
      return filterData
    }

    if (this.state.filterDay != 'all') {
      let completedTask = filterData.lanes[1].cards
      const filterDay = Number(this.state.filterDay)

      completedTask = completedTask.filter((item: { created_date: moment.MomentInput; }) => {
        const diffDay = moment().diff(moment(item.created_date), 'day')
        return diffDay < filterDay
      })
      filterData.lanes[1].cards = completedTask
    }

    // return filterData

    let statusFilter = this.state.status_filter.slice().split(',')
    let assignmentFilter = this.state.assignment_filter.slice().split(',')
    let filterLanes: any[] = []

    filterData.lanes.forEach((lane: { cards: any; }, index: string | number) => {
      let tmpCards = lane.cards
      if (this.state.status_filter != '' && statusFilter.length != 0) {
        tmpCards = tmpCards.filter((card: { customer_qb_id: string; }) => {
          if (statusFilter.indexOf(`'${card.customer_qb_id}'`) >= 0 || card.customer_qb_id === '')
            return card
        })
      }

      if (this.state.assignment_filter != '' && assignmentFilter.length != 0) {
        tmpCards = tmpCards.filter((card: { metadata: any[]; }) => {
          if (card.metadata.length != 0) {
            const findAssignment = card.metadata.filter((assignment: { key: any; }) => {
              if (assignmentFilter.indexOf(`'${assignment.key}'`) >= 0)
                return assignment
            })
            if (findAssignment.length > 0) {
              return card
            }

          }
        })
      }

      lane.cards = tmpCards
      filterLanes[index] = lane
    })

    filterData.lanes = filterLanes
    return filterData
  }

  dayFilterOnChangeHandler = (event: React.FormEvent<HTMLDivElement>, option?: IDropdownOption, index?: number) => {
    console.dir(option)
    this.setState({ filterDay: option.key })
  }

  updateTitle = (laneId, data) => {
    console.dir(laneId)
    console.dir(data.title)
    axios.put(`/Task/UpdateLaneTitle`, { id: laneId, title: data.title })
      .then((response) => {
        console.log(response)
        let newBoardLane = JSON.parse(JSON.stringify(this.state.boardLane))
        newBoardLane = newBoardLane.map(item => {
          if (item.id == laneId)
            return { ...item, title: data.title }
          else
            return item
        })

        let newData = JSON.parse(JSON.stringify(this.state.data))
        newData.lanes = newData.lanes.map(item => {
          if (item.id == laneId)
            return { ...item, title: data.title }
          else
            return item
        })
        console.dir(newData)
        this.setState({ boardLane: newBoardLane, data: newData })
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  async componentDidMount() {
    const isLogin = await LoginCheck()
    if (isLogin) {
      try {
        await this.getClientPhases()
        this.loadCustomer()
        // this.getBoard()
        // this.getCurrentUser().then(() => { console.log('get current user info') });

        //get all available users
        axios.defaults.headers.common = { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
        axios.get('/Task/GetTaskData')
          // axios.get('/MSGraph/Users')
          .then((response) => {
            // handle success
            let { users, boardLane } = response.data as any
            this.setState({ Users: users, boardLane: boardLane })
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(() => {
            // 
            let count = 0
            let filterMenu_ = []
            while (count < this.state.Users.length) {
              filterMenu_.push(false)
              count++
            }
            this.setState({ filterMenu1: filterMenu_ })
          });
      } catch (error) {
        LogOut()
      }
    }
  }

  private sessionStorageEventListener() {
    sessionStorageListener.init()
    sessionStorageListener.addListener('customer', (value: { fullyQualifiedNameField: any; topCustomer?: any; displayNameField?: string; levelField?: number; companyNameField?: string; idField?: string; primaryPhoneField?: { freeFormNumberField: string; }; mobileField?: { freeFormNumberField: string; }; faxField?: { freeFormNumberField: string; }; alternatePhoneField?: { freeFormNumberField: string; }; primaryEmailAddrField?: { addressField: string; }; webAddrField?: { uRIField: string; }; billAddrField?: { cityField: string; countrySubDivisionCodeField: string; line1Field: string; postalCodeField: string; }; shipAddrField?: { cityField: string; countrySubDivisionCodeField: string; line1Field: string; postalCodeField: string; }; parentRefField?: { nameField: string; typeField: string; valueField: string; }; note?: string; resaleNumField?: string; taxable?: boolean; preferredDeliveryMethodField?: string; activeField?: boolean; balanceWithJobsField?: number; }) => {
      const customer = value as customer;
      let fullyQualifiedNameField = value.fullyQualifiedNameField
      let phases_level2_ = this.state.phases_level2.slice()

      let filterMenu0Tmp = phases_level2_.map((item, index) => {
        if (value.topCustomer && item.key == value.topCustomer.Id)
          return true
        else
          return false
      })

      this.setState({
        task_loading: true,
        Customer: {
          qb_id: customer.idField ? customer.idField : '0',
          status_qb_id: value.topCustomer ? value.topCustomer.Id : '0',
          name: customer.displayNameField,
          status: fullyQualifiedNameField ? fullyQualifiedNameField.split(':')[0] + ' | ' + fullyQualifiedNameField.split(':')[1] : ""
        },
        filterMenu0: filterMenu0Tmp,
        filterMenu0Value: [value.topCustomer ? value.topCustomer.Id : ''],
        filterMenuNumber: [1, 0],
        filterNumber: 1,
        status_filter: `'${value.topCustomer ? value.topCustomer.Id : ''}'`,
        assignment_filter: "",
      }, () => {
        //load board
        setTimeout(this.getBoard.bind(this), 1)
      })
    })
  }

  private async loadCustomer() {
    let filterCustomer = localStorage.getItem('taskFilter')
    // let getLocalStorageCustomer = JSON.parse(localStorage.getItem('customer') as string)

    // let customerPromise = await QuickbooksApi.getCustomerById(getLocalStorageCustomer.Id)

    // const customer = customerPromise.Result as customer;

    // let fullyQualifiedNameField = customer.fullyQualifiedNameField
    let phases_level2_ = this.state.phases_level2.slice()

    // let filterMenu0Tmp = phases_level2_.map((item, index) => {
    //   if (getLocalStorageCustomer.topCustomer && item.key == getLocalStorageCustomer.topCustomer.Id)
    //     return true
    //   else
    //     return false
    // })

    let filterMenu0Tmp = phases_level2_.map((item, index) => {
      return item.key == filterCustomer ? true : false
    })

    let filterMenuNumber0 = 0
    if (filterCustomer) {
      filterMenuNumber0 = 1
    }

    console.dir(filterCustomer)


    this.setState({
      task_loading: true,
      Customer: {
        qb_id: '',
        // status_qb_id: getLocalStorageCustomer.topCustomer ? getLocalStorageCustomer.topCustomer.Id : '0',
        status_qb_id: '0',
        name: '',
        // status: fullyQualifiedNameField ? fullyQualifiedNameField.split(':')[0] + ' | ' + fullyQualifiedNameField.split(':')[1] : ""
        status: ''
      },
      filterMenu0: filterMenu0Tmp,
      // filterMenu0Value: [getLocalStorageCustomer.topCustomer ? getLocalStorageCustomer.topCustomer.Id : ''],
      filterMenu0Value: filterCustomer ? [filterCustomer] : [],
      filterMenuNumber: [filterMenuNumber0, 0],
      filterNumber: filterCustomer ? 1 : 0,
      // status_filter: `'${getLocalStorageCustomer.topCustomer ? getLocalStorageCustomer.topCustomer.Id : ''}'`,
      status_filter: filterCustomer ? `'${filterCustomer}'` : "",
      assignment_filter: "",
    }, () => {
      //load board
      setTimeout(this.getBoard.bind(this), 1)
    })

    if (filterCustomer)
      localStorage.removeItem('taskFilter')
  }

  public render(): React.ReactElement<IQboTasksProps> {
    let users = this.state.Users.slice()
    let order = 0
    let usersItem = []

    while (order < users.length) {
      let val = {
        key: "assign" + order,
        data: users[order].id,
        text: users[order].displayName,
        canCheck: true,
        isChecked: this.state.filterMenu1[order],
        onClick: this.onToggleSelect,
      }
      usersItem.push(val)
      order++
    }

    let phase_level1 = this.state.phases_level1.slice()
    let phase_level2 = this.state.phases_level2.slice()
    let phase_level1_ = []
    let phase_level2_ = []

    //console.log(phase_level2)
    // for (let i = 0; i < phase_level1.length; i++) {
    let j = 0
    while (j < phase_level2.length) {
      // if (phase_level2[j].parent_id == phase_level1[i].key) {
      let lvl2 = {
        key: "status" + j + '|' + phase_level2[j].key,
        text: phase_level2[j].text,
        canCheck: true,
        isChecked: this.state.filterMenu0[j],
        onClick: this.onToggleSelect
      }
      phase_level2_.push(lvl2)
      // }
      j++
    }
    //   let lvl1 = {
    //     key: phase_level1[i].key,
    //     text: phase_level1[i].text,
    //     subMenuProps: { items: phase_level2_ }
    //   }
    //   phase_level1_.push(lvl1)
    //   phase_level2_ = []
    // }

    const menuProps: IContextualMenuProps = {
      shouldFocusOnMount: true,
      delayUpdateFocusOnHover: false,
      directionalHint: DirectionalHint.bottomRightEdge,
      items: [
        {
          key: 'filterMenu0', text: 'Customer (' + this.state.filterMenuNumber[0] + ')', canCheck: false,
          subMenuProps: {
            items: phase_level2_
          }
        },
        {
          key: 'filterMenu1', text: 'Assignment (' + this.state.filterMenuNumber[1] + ')', canCheck: false,
          subMenuProps: {
            items: usersItem,
          }
        }
      ]
    }
    return (
      <Fragment>
        <div>
          <div>
            <div>
              <div style={{ top: '-2px' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', background: 'white' }}>
                  {/* <div className={this.state.Customer.name == '' || this.state.Customer.name == null ? '' : styles.customerTitleName}>{this.state.Customer.name}</div> */}

                  <div style={{ display: 'flex' }}>

                    <CommandBarButton
                      text={"Filter (" + this.state.filterNumber + ")"}
                      style={{ height: '44px', backgroundColor: 'white', border: '0px', marginRight: '50px' }}
                      menuProps={menuProps}
                      disabled={false}
                      checked={false}
                    />
                  </div>
                </div>
                {
                  // (this.state.Customer.status as any).indexOf('|') >= 0
                  //   ? <div style={{ display: 'flex' }}>
                  //     <div className={styles.customerTitleStatus}>{(this.state.Customer.status as any).split('|')[0]}</div>
                  //     <div className={styles.customerTitleStatus}>{(this.state.Customer.status as any).split('|')[1]}</div>
                  //   </div>
                  //   : <></>
                }
              </div>
            </div>
          </div>

          <Board style={{ backgroundColor: 'white', height: 'auto' }} data={this.filterBoard()}
            eventBusHandle={this.setEventBus}
            editable={this.state.editable}
            hideCardDeleteIcon
            onCardClick={this.clickCard}
            onCardAdd={this.addCard}
            handleDragEnd={this.dragCardEnd}
            customer={this.state.Customer}
            components={{
              NewCardForm: NewCardForm,
              Card: CustomCard
            }}
            editLaneTitle
            onLaneUpdate={(laneId, data) => this.updateTitle(laneId, data)}
          />
          {this.state.task_loading ? <Loading /> : null}
          <CardModal
            key={this.state.Card.id}
            opened={this.state.opened}
            closePopup={this.onPopupClosed}
            lane_id={this.state.Card.lane_id}
            card_id={this.state.Card.id}
            status_qb_id={this.state.Card.status_qb_id}
            title={this.state.Card.title}
            description={this.state.Card.description}
            priority={this.state.Card.priority}
            metadata={this.state.Card.metadata}
            saveRecipients={this.saveRecipients}
            alert_recipients={this.state.alert_recipients}
            label={this.state.Card.label}
            due_date={this.state.Card.due_date}
            is_template={this.state.Card.is_template}
            reminder_id={this.state.Card.reminder_id}
            attachments={this.state.attachments}
            users={this.state.unassignedUsers}
            addMember={this.addMember}
            deleteMember={this.deleteMember}
            addMention={this.addMention}
            deleteMention={this.deleteMention}
            comments={this.state.comments}
            checklist={this.state.checklist}
            addComment={this.addComment}
            typingComment={this.state.typingComment}
            typingChecklist={this.state.typingChecklist}
            changed={this.typeComment}
            saveCard={this.saveCard}
            customerName={this.getQuickbooksCustomerName(this.state.Card.customer_qb_id)}
            customerStatus={this.state.Customer.status}
            hidePersona={this.state.hidePersona}
            addCommentBtnDisabled={this.state.addCommentBtnDisabled}
            addChecklist={this.addChecklist}
            editChecklist={this.editChecklist}
            arrangeChecklist={this.arrangeChecklist}
            deleteChecklist={this.deleteChecklist}
            makeTemplate={this.makeTemplate}
            phases={this.state.phases_level2}
            attachmentLoading={this.state.attachmentLoading}
            attachmentLoadingBegins={this.attachmentLoadingBegins}
            addAttachment={this.addAttachment}
            deleteAttachment={this.deleteAttachment}
            deleteComment={this.deleteComment}
            customer={this.state.Customer}
            boardLane={this.state.boardLane}
          />
        </div>
      </Fragment>
    );
  }
}


const dropdownStyles: Partial<IDropdownStyles> = {
  dropdown: { width: 300 },
};

const options: IDropdownOption[] = [
  { key: 'all', text: 'All' },
  { key: '30', text: '30 days' },
  { key: '60', text: '60 days' },
  { key: '90', text: '90 days' },
];