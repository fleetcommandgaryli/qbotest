export interface IQboCustomerStates {
  accessToken: string;
  vendors: Array<Vendor>;
  employees: Array<Employee>;
  item: Array<Item>;
  resourceCustomers: Array<customer>;
  customers: Array<customer>;
  customerType: [];
  taxCode: [];
  paymentMethod: [];
  term: [];
  selectCustomer: customer;
  timeActivity: Array<TimeActivity>;
  subId: string;
  searchCustomer: string;
  selectTimeActivity: TimeActivity;
  selectEnterStartAndEndTime: boolean;
  timeActivityEdit: boolean;
  notify: any;
  pages: number;
  initLoading: boolean;
  timeActivityLoading: boolean;
  isDeleteModalOpen: boolean;
  isInactiveModalOpen: boolean;
  payRate : any;
  timeActivityPayRate: any;
  currentUser: any;
  includeInactive: boolean;
  customerCount: number;
  allSharePointUsers: [];
  customersTag: [];
  invoices: [];
  customerDropDown: Array<customer>;
  tmpItem: any;
  isItemSelectModalOpen: boolean;
  isTimerModalOpen: boolean;
}

export type Vendor = {
  Id: string;
  DisplayName: string;
}

export type Employee = {
  idField: string;
  displayNameField: string;
  familyNameField: string;
  middleNameField: string;
  givenNameField: string;
  activeField: string
  primaryEmailAddrField: any;
}

export type SharePointUsers = {
  id: string;
  displayName: string;
  imageInitials: string;
  mail: string;
  text: string;
  userPrincipalName: string;
  givenName: string;
  surname: string;
}

export type Item = {
  Id: string;
  Name: string;
  FullyQualifiedName: string;
  UnitPrice: string;
  Active: string;
  Taxable: string;
  Type: string;
  SyncToken: string;
  Description: string;
}

export type QueryResponse = {
  QueryResponse: QueryResponseDetail;
}

type QueryResponseDetail = {
  Vendor: Array<Vendor>;
  Employee: Array<Employee>;
  Item: Array<Item>
}

export type RefreshToken = {
  quickBooksInfo: QuickBooksInfo
  tokenResponse: TokenResponse
}

export type QuickBooksInfo = {
  access_token: string;
  client_id: string;
}

type TokenResponse = {
  IsError: boolean;
}

export type CustomerResponse = {
  Result: Array<customer>;
}

export type customer = {
  displayNameField: string;
  levelField: number;
  companyNameField: string;
  fullyQualifiedNameField: string;
  idField: string;
  primaryPhoneField: phoneField;
  mobileField: phoneField;
  faxField: phoneField;
  alternatePhoneField: phoneField;
  primaryEmailAddrField: primaryEmailAddrField;
  webAddrField: webAddrField;
  billAddrField: billAddrField;
  shipAddrField: billAddrField;
  parentRefField: parentRefField;
  note: string;
  resaleNumField: string;
  taxable: boolean;
  preferredDeliveryMethodField: string;
  activeField: boolean;
  balanceWithJobsField: number;
}

type phoneField = {
  freeFormNumberField: string;
}

type primaryEmailAddrField = {
  addressField: string;
}

type webAddrField = {
  uRIField: string;
}

type billAddrField = {
  cityField: string;
  countrySubDivisionCodeField: string;
  line1Field: string;
  postalCodeField: string;
}

type parentRefField = {
  nameField: string;
  typeField: string;
  valueField: string;
}

export type TimeActivityQueryResponse = {
  QueryResponse: TimeActivityQueryResponseDetail;
}

type TimeActivityQueryResponseDetail = {
  TimeActivity: Array<TimeActivity>;
}

export type TimeActivity = {
  Description: string;
  HourlyRate: string;
  Hours: string;
  Minutes: string;
  NameOf: string;
  SyncToken: string;
  Taxable: string;
  TxnDate: string;
  EmployeeRef: QuickbooksRefernaceType;
  ItemRef: QuickbooksRefernaceType;
  VendorRef: QuickbooksRefernaceType;
  DepartmentRef: QuickbooksRefernaceType;
  CustomerRef: QuickbooksRefernaceType;
  Id: string;
  StartTime: string;
  EndTime: string;
  computedStartTime: string;
  computedEndTime: string;
  BillableStatus: string;
  time: string;
  emailPayRate: any;
}

type QuickbooksRefernaceType = {
  name: string;
  value: string;
}

export type CustomerModal = {
  customer: customer;
}